// ----------------------------------------------------------------------------
// Radish Hairworks UI: additional animations for preview
// ----------------------------------------------------------------------------
function RADHW_getPreviewAnimations() : array<name>
{
    var entries: array<name>;

    // some anims for woman and man
    entries.PushBack('high_standing_determined_enter_frontal');
    entries.PushBack('ciri_drinks_vodka');
    entries.PushBack('woman_adjusting_clothes_loop_3');
    entries.PushBack('high_standing_determined_exit_rightside');
    // add others if you need some

    return entries;
}
// ----------------------------------------------------------------------------
