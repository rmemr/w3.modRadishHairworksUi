// ----------------------------------------------------------------------------
// Radish Hairworks UI: additional texture list entries can be added here
// ----------------------------------------------------------------------------
function RADHW_getCustomTextureListEntries(listId: String) : array<String>
{
    // listId will specify the textures list that will be extend
    // Note: fallthrough case statements (= missing break) will add textures to
    // multiple lists
    var entries: array<String>;

    switch (listId) {
        case "StiffnessTextures":
            entries.PushBack("characters\models\monsters\bies\model\bies_stiffness.xbm");
            entries.PushBack("characters\models\monsters\bies\model\bies_rootstiffness.xbm");
            break;
        case "VolumeTextures":
            entries.PushBack("characters\models\monsters\bies\model\bies_density.xbm");
            break;
        case "WidthTextures":
        case "ClumpTextures":
            entries.PushBack("characters\models\monsters\bies\model\bies_clump.xbm");
            break;
        case "WaveTextures":
            break;
        case "ColorTextures":
        case "StrandTextures":
            entries.PushBack("characters\models\monsters\bies\model\bies_rootcolor.xbm");
            entries.PushBack("characters\models\monsters\bies\model\bies_tipcolor.xbm");
            break;
    }

    return entries;
}
// ----------------------------------------------------------------------------
