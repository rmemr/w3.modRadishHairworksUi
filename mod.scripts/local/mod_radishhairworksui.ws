// ----------------------------------------------------------------------------
state CreateRadishHairworksUiMod in CModBootstrap extends BootstrapCreateMod {
    // ------------------------------------------------------------------------
    final function modCreate(): CMod {
        return new CRadishHairworksUiMod in parent;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
exec function hairworksui() {
    var mod: CRadishHairworksUiMod;
    mod = new CRadishHairworksUiMod in thePlayer;
    mod.init();
}
// ----------------------------------------------------------------------------
class CRadHwPopupCallback extends IModUiConfirmPopupCallback {
    public var callback: CRadishHairworksUiMod;

    public function OnConfirmed(action: String) {
        switch (action) {
            case "quit": return callback.doQuit();
        }
    }
}
// ----------------------------------------------------------------------------
state RadHW_Active in CRadishHairworksUiMod {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var s: SRadishPlacement;
        var distance: float;

        parent.modUtil.deactivateHud();
        parent.modUtil.hidePlayer();
        parent.modUtil.freezeTime();

        // refresh camera position to player position
        s.pos = thePlayer.GetWorldPosition();
        s.rot = thePlayer.GetWorldRotation();
        s.rot.Pitch -= 25;
        s.rot.Roll = 0;
        distance = -5.0;
        s.pos.X -= distance * SinF(Deg2Rad(s.rot.Yaw - 15));
        s.pos.Y += distance * CosF(Deg2Rad(s.rot.Yaw - 15));
        s.pos.Z += 4.0;
        s.pos.W = 1.0;

        parent.configManager.setLastCamPosition(s);

        if (!parent.radHwEditor.activate()) {
            // something went wrong (e.g. no hairworks entities found)
            parent.GotoState('RadHW_Sleeping', false, true);
        }
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.SetContext('Exploration');
        parent.radHwEditor.deactivate();

        parent.modUtil.unfreezeTime();
        parent.modUtil.restorePlayer();
        parent.modUtil.reactivateHud();
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RADHW_Minimize') {
                parent.GotoState('RadHW_Sleeping', false, true);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadHW_Sleeping in CRadishHairworksUiMod {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnMaximize(action: SInputAction) {
        var entity : CEntity;
        var template : CEntityTemplate;

        if (IsPressed(action)) {
            if (!parent.radHwEditor) {
                parent.radHwEditor = new CRadishHairworksEditor in this;
                parent.radHwEditor.init(parent.log, parent.configManager, parent.quitConfirmCallback);

                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_modutils.w2ent", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());
                parent.modUtil = (CRadishModUtils)entity;
            }

            parent.PushState('RadHW_Active');
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CRadishHairworksUiMod extends CMod {
    default modName = 'RadishHairworksUi';
    default modAuthor = "rmemr";
    default modUrl = "http://www.nexusmods.com/witcher3/mods/3620/";
    default modVersion = '0.4';

    default logLevel = MLOG_DEBUG;
    // ------------------------------------------------------------------------
    protected var radHwEditor: CRadishHairworksEditor;
    protected var modUtil: CRadishModUtils;
    // ------------------------------------------------------------------------
    // UI stuff
    protected var quitConfirmCallback: CRadHwPopupCallback;
    // ------------------------------------------------------------------------
    protected var configManager: CRadishHairworksUiConfigManager;
    // ------------------------------------------------------------------------
    public function init() {
        super.init();

        configManager = new CRadishHairworksUiConfigManager in this;
        configManager.init();

        this.registerListeners();

        // prepare view callback wiring
        quitConfirmCallback = new CRadHwPopupCallback in this;
        quitConfirmCallback.callback = this;

        PushState('RadHW_Sleeping');
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {}
    event OnMaximize(action: SInputAction) {}
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnMinimize', 'RADHW_Minimize');
        theInput.RegisterListener(this, 'OnMaximize', 'RADHW_Maximize');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADHW_Minimize');
        theInput.UnregisterListener(this, 'RADHW_Maximize');
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        var null: CRadishHairworksEditor;

        this.radHwEditor.doQuit();
        this.radHwEditor = null;
        PopState();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
