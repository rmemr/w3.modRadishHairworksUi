// ----------------------------------------------------------------------------
struct SHWUI_Setting {
    var cat1: String;
    var cat2: String;
    var cat3: String;
    var sid: String;
    var caption: String;
    var stepSize: float;
    var min: float;
    var max: float;
}
// ----------------------------------------------------------------------------
enum EHWUI_EmptySettingFilter {
    EHWUI_ESF_NONE = 0,
    EHWUI_ESF_MARK = 1,
}
// ----------------------------------------------------------------------------
class CModHairworksUiList extends CModUiFilteredList {
    private var settings: array<SHWUI_Setting>;
    private var fullList: array<SModUiCategorizedListItem>;

    private var filterType: EHWUI_EmptySettingFilter;
    // ------------------------------------------------------------------------
    public function initList() {
        var data: C2dArray;
        var i: int;

        data = LoadCSV("dlc/dlcradishhairworksui/data/hairworksui_settings.csv");

        items.Clear();
        // csv: CAT1;CAT2;CAT3;id;caption
        for (i = 0; i < data.GetNumRows(); i += 1) {
            settings.PushBack(SHWUI_Setting(
                data.GetValueAt(0, i),
                data.GetValueAt(1, i),
                data.GetValueAt(2, i),
                data.GetValueAt(3, i),
                data.GetValueAt(4, i),
                StringToFloat(data.GetValueAt(5, i), 0),
                StringToFloat(data.GetValueAt(6, i), 0),
                StringToFloat(data.GetValueAt(7, i), 0),
            ));
            fullList.PushBack(SModUiCategorizedListItem(
                i,
                settings[i].caption,
                settings[i].cat1,
                settings[i].cat2,
                settings[i].cat3
            ));
        }

        items = fullList;
        filterType = EHWUI_ESF_MARK;
    }
    // ------------------------------------------------------------------------
    public function getEmptySettingsFilter() : EHWUI_EmptySettingFilter {
        return this.filterType;
    }
    // ------------------------------------------------------------------------
    public function filterEmptySettings(hwParams: CModHairworksUiParams, filter: EHWUI_EmptySettingFilter) {
        var value: SHWUI_Value;
        var i, s: int;
        var skipCategory: String;
        var removeEmpty: bool;

        filterType = filter;

        switch (filterType) {
            case EHWUI_ESF_MARK: removeEmpty = false; break;

            case EHWUI_ESF_NONE:
            default:
                items = fullList;
                return;
        }

        skipCategory = "-";

        s = fullList.Size();
        items.Clear();

        for (i = 0; i < s; i += 1) {
            value = hwParams.getValue(settings[i].cat1, settings[i].sid);

            if (settings[i].sid == "activated" && !value.b) {
                skipCategory = settings[i].cat1;
            }

            if (settings[i].cat1 != skipCategory) {
                skipCategory = "-";
                items.PushBack(fullList[i]);
                continue;
            }

            if (!removeEmpty) {
                items.PushBack(
                    SModUiCategorizedListItem(
                        i,
                        "<font color=\"#666666\">" + settings[i].caption + "</font>",
                        settings[i].cat1,
                        settings[i].cat2,
                        settings[i].cat3
                    )
                );
            }
        }
    }
    // ------------------------------------------------------------------------
    public function refreshFilter(hwParams: CModHairworksUiParams) {
        filterEmptySettings(hwParams, filterType);
    }
    // ------------------------------------------------------------------------
    public function getMeta(id: int) : SHWUI_Setting {
        return settings[id];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
