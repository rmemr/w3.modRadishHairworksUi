// ----------------------------------------------------------------------------
// wrapper for an entity with multiple fur components
class CModHairworksUiFurEntity {
    private var entity: CEntity;
    private var currentCompSlot: int;
    private var currentFurComp: CFurComponent;
    private var furCompCount: int;
    private var furCenterPoint: Vector;
    // ------------------------------------------------------------------------
    private var anims: array<name>;
    private var currentAnimSlot: int;
    private var hasWetnessComp: bool;
    // ------------------------------------------------------------------------
    public function init(entity: CEntity) {
        var cmps: array<CComponent>;

        // check for wetness component to enable wetness editing
        cmps = entity.GetComponentsByClassName('CWetnessComponent');
        if (cmps.Size() > 0) {
            hasWetnessComp = true;
            // reduce blend time
            // doesn't seem to have any effect
            // wetnessComp = (CWetnessComponent)cmps[0];
            // wetnessComp.blendInFromOcean = 0.1;
            // wetnessComp.blendInFromRain = 0.1;
            // wetnessComp.blendOutFromOcean = 0.1;
            // wetnessComp.blendOutFromRain = 0.1;
        } else {
            hasWetnessComp = false;
        }

        cmps.Clear();
        cmps = entity.GetComponentsByClassName('CFurComponent');

        this.entity = entity;
        this.furCompCount = cmps.Size();
        selectSlot(0);
        initCenterPoint();

        currentAnimSlot = 0;
        anims = RADHW_getPreviewAnimations();
    }
    // ------------------------------------------------------------------------
    public function playExampleAnimation() : bool {
        var animatedComponent: CAnimatedComponent;
        var success: Bool;
        var i, count: int;

        animatedComponent = (CAnimatedComponent)((CActor)entity)
            .GetComponentByClassName('CAnimatedComponent');

        count = anims.Size();

        if (count > 0) {
            for (i = 0; i < count; i += 1) {

                success = animatedComponent.PlaySlotAnimationAsync(anims[currentAnimSlot], 'NPC_ANIM_SLOT');
                currentAnimSlot = (currentAnimSlot + 1) % count;

                if (success) {
                    return true;
                }
            }
        }

        return success;
    }
    // ------------------------------------------------------------------------
    public function refresh() {
        var copy: CEntity;
        var actor: CActor;
        var appearance: CName;
        var animatedComponent: CAnimatedComponent;

        copy = entity.Duplicate();

        actor = (CActor)copy;
        if (actor) {
            appearance = ((CActor)entity).GetAppearance();
            actor.SetAppearance(appearance);
            actor.SetTemporaryAttitudeGroup('q104_avallach_friendly_to_all', AGP_Default);

            animatedComponent = (CAnimatedComponent)entity.GetComponentByClassName('CAnimatedComponent');
            if (animatedComponent && animatedComponent.HasFrozenPose()) {
                animatedComponent = (CAnimatedComponent)actor.GetComponentByClassName('CAnimatedComponent');
                animatedComponent.FreezePose();
            }
        }
        entity.Destroy();

        entity = copy;
        // refresh cached reference
        selectSlot(currentCompSlot);
    }
    // ------------------------------------------------------------------------
    public function getCenterpointSettings() : Vector {
        return furCenterPoint;
    }
    // ------------------------------------------------------------------------
    public function setCenterpointSettings(settings: Vector) {
        furCenterPoint = settings;
    }
    // ------------------------------------------------------------------------
    private function initCenterPoint() {
        var box: Box;
        var prevDistance: float;

        // center around fur component, assuming fur is mostly on top (e.g. hairstyles)
        box = currentFurComp.GetBoundingBox();

        furCenterPoint = entity.GetWorldPosition();
        furCenterPoint.Z += 1.6;
        furCenterPoint.W = prevDistance;
    }
    // ------------------------------------------------------------------------
    private function selectSlot(slot: int) {
        var cmps: array<CComponent>;

        cmps = entity.GetComponentsByClassName('CFurComponent');

        this.currentCompSlot = slot;
        this.currentFurComp = (CFurComponent)cmps[currentCompSlot];
    }
    // ------------------------------------------------------------------------
    public function hasWetnessComp() : bool {
        return this.hasWetnessComp;
    }
    // ------------------------------------------------------------------------
    public function getCurrentFurComponent(): CFurComponent {
        return currentFurComp;
    }
    // ------------------------------------------------------------------------
    public function getNextFurComponent(): CFurComponent {
        currentCompSlot = (currentCompSlot + 1) % furCompCount;

        selectSlot(currentCompSlot);
        initCenterPoint();

        return currentFurComp;
    }
    // ------------------------------------------------------------------------
    public function getPreviousFurCoponent(): CFurComponent {
        currentCompSlot = (furCompCount + currentCompSlot - 1) % furCompCount;

        selectSlot(currentCompSlot);
        initCenterPoint();

        return currentFurComp;
    }
    // ------------------------------------------------------------------------
    public function getCurrentComponentListCaption(): String {
        // TODO add entity name (split template path, remove w2ent)

        return IntToString(currentCompSlot + 1) + "/" + IntToString(furCompCount) + " "
            + StrReplaceAll(currentFurComp.GetName(), "_", " ");
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// wrapper for all entities found for editing
class CModHairworksUiFurEntityPool {
    private var templatePaths: array<String>;
    private var currentEntity: CModHairworksUiFurEntity;
    // ------------------------------------------------------------------------
    public function rescan(): int {
        var null: CModHairworksUiFurEntity;
        var entities: array<CEntity>;
        var cmps: array<CComponent>;
        var i, c: int;
        var entity: CEntity;
        var path: String;

        this.templatePaths.Clear();
        this.currentEntity = null;

        // for now only entities tagged with "hairworks"
        theGame.GetEntitiesByTag('hairworks', entities);

        for (i = 0; i < entities.Size(); i += 1) {
            entity = entities[i];

            cmps = entity.GetComponentsByClassName('CFurComponent');

            if (cmps.Size() > 0) {
                //path = entity.template.GetPath();
                path = entity.GetReadableName();

                if (templatePaths.Size() == 0) {
                    // select first one
                    currentEntity = new CModHairworksUiFurEntity in this;
                    currentEntity.init(entity);
                }

                if (!templatePaths.Contains(path)) {
                    templatePaths.PushBack(path);
                }
            }
        }

        return templatePaths.Size();
    }
    // ------------------------------------------------------------------------
    public function getCurrent(): CModHairworksUiFurEntity {
        return currentEntity;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
