// ----------------------------------------------------------------------------
state RadHw_Active in CRadishHairworksEditor {
    // ------------------------------------------------------------------------
    private var entity: CModHairworksUiFurEntity;
    // ------------------------------------------------------------------------
    // alias
    private var hwParams: CModHairworksUiParams;
    private var listProvider: CModHairworksUiList;

    private var selectedSetting: SHWUI_Setting;
    private var stepSize: float; default stepSize = 0.1;
    // ------------------------------------------------------------------------
    private var isStepSizeChange: bool;
    private var isValueEditing: bool;
    private var isDirectEditOnly: bool;

    private var editedValueType: EHWUI_ValueType;
    private var editWetnessVersion: bool; default editWetnessVersion = false;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        this.hwParams = parent.hwParams;
        this.listProvider = parent.listProvider;

        theInput.StoreContext('MOD_HairworksUi');
        registerListeners();

        entity = parent.furEntities.getCurrent();
        hwParams.setComponent(entity.getCurrentFurComponent(), editWetnessVersion);

        listProvider.refreshFilter(hwParams);
        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        if (prevStateName == 'RadHw_Sleeping') {
            listProvider.setSelection(0, true);
            selectedSetting = listProvider.getMeta(0);
            isDirectEditOnly = false;
        }

        updateHwCaption();
        parent.showUi(true);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        unregisterListeners();
    }
    // ------------------------------------------------------------------------
    event OnInteractiveCam(action: SInputAction) {
        if (IsReleased(action)) {
            parent.showUi(false);
            parent.PushState('RadHw_InteractiveCamera');
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_SetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ResetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ListCategoryUp'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_LogValues'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ToggleUi'));

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_EditCurrentValue'));

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeValue'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeValueP1'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeValueP2'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeValueP3'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeValueP4'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_PlayAnim'));

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_AdjustTime'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ToggleInteractiveCam', 'RAD_ToggleInteractiveCam', IK_LControl));

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ToggleEmptyVisibility'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ChangeFurComponent'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ToggleDryWetParamEditing'));

        hotkeyList.PushBack(HotkeyHelp_from('RADHW_LogValues'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_Minimize'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_Quit'));
    }
    // ------------------------------------------------------------------------
    // called by user action to start filter input
    event OnFilter(action: SInputAction) {
        if (!parent.view.listMenuRef.isEditActive() && IsPressed(action)) {

            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("RADHW_lFilter"), listProvider.getWildcardFilter());
        }
    }
    // ------------------------------------------------------------------------
    // called by user action to reset currently set filter
    event OnResetFilter() {
        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        updateView();
    }
    // ------------------------------------------------------------------------
    // called by user action to go one opened category up
    event OnCategoryUp(action: SInputAction) {
        if (IsPressed(action)) {
            listProvider.clearLowestSelectedCategory();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnCycleSelection(action: SInputAction) {
        var prev: bool;
        var id: String;

        if (action.value != 0) {
            prev = action.value < 0;

            if (prev) {
                id = listProvider.getPreviousId();
            } else {
                id = listProvider.getNextId();
            }
            OnSelected(id);
        }
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputCancel() {
        parent.notice(GetLocStringByKeyExt("RADHW_iEditCanceled"));

        isValueEditing = false;
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputEnd(inputString: String) {
        if (inputString == "") {
            OnResetFilter();
        } else {
            if (isValueEditing) {
                parseAndSetValue(inputString);
                parent.view.listMenuRef.resetEditField();
            } else {
                // Note: filter field is not removed to indicate the current filter
                listProvider.setWildcardFilter(inputString);
            }
            updateView();
        }
        isValueEditing = false;
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnSelected(listItemId: String) {
        // listprovider opens a category if a category was selected otherwise
        // returns true (meaning a "real" item was selected)
        if (listProvider.setSelection(listItemId, true)) {
            selectedSetting = listProvider.getMeta(StringToInt(listItemId));
            isDirectEditOnly = selectedSetting.stepSize == -1
                && selectedSetting.min == -1
                && selectedSetting.max == -1;

            if (selectedSetting.stepSize == 0) {
                stepSize = 0.5;
            } else {
                stepSize = selectedSetting.stepSize;
            }
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview (when list menu opens)
    event OnUpdateView() {
        var wildcard: String;
        // Note: if search filter is active show the wildcard to indicate the
        // current filter
        wildcard = listProvider.getWildcardFilter();
        if (wildcard != "") {
            parent.view.listMenuRef.setInputFieldData(
                GetLocStringByKeyExt("RADHW_lFilter"), wildcard);
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        var settingsStepSize: float;

        if (selectedSetting.stepSize == 0) {
            settingsStepSize = 0.5;
        } else {
            settingsStepSize = selectedSetting.stepSize;
        }

        if (IsPressed(action)) {
            if (action.aName == 'RAD_ToggleFast') {
                stepSize = settingsStepSize * 4;
            } else {
                stepSize = settingsStepSize / 4;
            }
        } else if (IsReleased(action)) {
            stepSize = settingsStepSize;
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // value adjustments
    event OnChangeValue(action: SInputAction) {
        var t: EHWUI_AdjustType;
        var v: SHWUI_Value;

        if (action.value != 0) {

            if (isDirectEditOnly) {
                parent.error(GetLocStringByKeyExt("RADHW_eOnlyDirectSelection"));
            } else if (selectedSetting.cat1 == "pins"
                || selectedSetting.cat1 == "bonespheres"
                || (selectedSetting.cat1 == "" && selectedSetting.sid == "autoHideDistance"))
            {
                parent.error(GetLocStringByKeyExt("RADHW_eReadOnlySetting"));
            } else {
                switch (action.aName) {
                    case 'RADHW_ChangeValue':     t = EHWUI_SCALAR; break;
                    case 'RADHW_ChangeValueP1':   t = EHWUI_VEC1; break;
                    case 'RADHW_ChangeValueP2':   t = EHWUI_VEC2; break;
                    case 'RADHW_ChangeValueP3':   t = EHWUI_VEC3; break;
                    case 'RADHW_ChangeValueP4':   t = EHWUI_VEC4; break;
                }

                v = hwParams.adjustValue(
                    selectedSetting.cat1, selectedSetting.sid, action.value * stepSize, t,
                    selectedSetting.min, selectedSetting.max);

                entity.refresh();

                updateCurrentValueCaption();

                // since bools may toggle filtering of empty categories ("activated")
                // update filter
                if (v.type == EHWUI_BOOL) {
                    listProvider.refreshFilter(hwParams);
                    updateView();
                }
            }
        }
    }
    // ------------------------------------------------------------------------
    private function updateCurrentValueCaption() {
        parent.view.listMenuRef.setExtraField(
                hwParams.getFormattedValue(selectedSetting.cat1, selectedSetting.sid),
                "#aaaaaa");
    }
    // ------------------------------------------------------------------------
    private function strToFloat(str: String, out f: float) : bool {
        f = StringToFloat(str, -666.6);
        return f != -666.6;
    }
    // ------------------------------------------------------------------------
    private function strToInt(str: String, out i: int) : bool {
        i = StringToInt(str, -666);
        return i != -666;
    }
    // ------------------------------------------------------------------------
    private function strToColor(str: String, out c: SHWUI_Color) : bool
    {
        var success: bool;
        var r, g, b, a: int;
        var remaining, s1, s2, s3, s4: String;

        success = StrSplitFirst(str, " ", s1, remaining);
        success = success && StrSplitFirst(remaining, " ", s2, remaining);
        success = success && StrSplitFirst(remaining, " ", s3, s4);

        success = success && strToInt(s1, r);
        success = success && strToInt(s2, g);
        success = success && strToInt(s3, b);
        success = success && strToInt(s4, a);

        c.r = r;
        c.g = g;
        c.b = b;
        c.a = a;

        return success;
    }
    // ------------------------------------------------------------------------
    private function strToVector(str: String, out v: Vector) : bool
    {
        var success: bool;
        var x, y, z, w: float;
        var remaining, s1, s2, s3, s4: String;

        success = StrSplitFirst(str, " ", s1, remaining);
        success = success && StrSplitFirst(remaining, " ", s2, remaining);
        success = success && StrSplitFirst(remaining, " ", s3, s4);

        success = success && strToFloat(s1, x);
        success = success && strToFloat(s2, y);
        success = success && strToFloat(s3, z);
        success = success && strToFloat(s4, w);

        v.X = x;
        v.Y = y;
        v.Z = z;
        v.W = w;

        return success;
    }
    // ------------------------------------------------------------------------
    private function parseValueStr(
        str: String, type: EHWUI_ValueType, out result: SHWUI_Value) : bool
    {
        var success: bool;

        switch (type) {
            case EHWUI_VECTOR:
                success = strToVector(str, result.vec);
                break;

            case EHWUI_COLOR:
                success = strToColor(str, result.c);
                break;

            case EHWUI_FLOAT:
                success = strToFloat(str, result.f);
                break;

            case EHWUI_INT:
                success = strToInt(str, result.i);
                break;
        }
        return success;
    }
    // ------------------------------------------------------------------------
    private function parseAndSetValue(inputString: String) {
        var newValue: SHWUI_Value;

        if (parseValueStr(inputString, editedValueType, newValue)) {
            hwParams.setCurrentValue(selectedSetting.cat1, selectedSetting.sid, newValue);

            entity.refresh();
        } else {
            parent.error(GetLocStringByKeyExt("RADHW_eValueParseErr"));
        }
    }
    // ------------------------------------------------------------------------
    event OnEditValue(action: SInputAction) {
        var strValue: String;
        var value: SHWUI_Value;
        var null: SHWUI_Setting;
        var listSetting: CRadHwUiListSetting;

        if (IsPressed(action)
            && selectedSetting != null
            && !parent.view.listMenuRef.isEditActive())
        {
            value = hwParams.getCurrentValue(selectedSetting.cat1, selectedSetting.sid);

            switch (value.type) {
                case EHWUI_VECTOR:
                    strValue = NoTrailZeros(value.vec.X)
                        + " " + NoTrailZeros(value.vec.Y)
                        + " " + NoTrailZeros(value.vec.Z)
                        + " " + NoTrailZeros(value.vec.W);
                    break;

                case EHWUI_COLOR:
                    strValue = IntToString(value.c.r)
                        + " " + IntToString(value.c.g)
                        + " " + IntToString(value.c.b)
                        + " " + IntToString(value.c.a);
                    break;

                case EHWUI_FLOAT:    strValue = NoTrailZeros(value.f); break;
                case EHWUI_INT:      strValue = IntToString(value.i); break;
                case EHWUI_BOOL:     return false; // doesn't make sense to "edit" bools
                case EHWUI_STRING:
                    switch (selectedSetting.sid) {
                        case "stiffnessTex":
                        case "rootStiffnessTex":
                        case "densityTex":
                        case "lengthTex":
                        case "rootWidthTex":
                        case "tipWidthTex":
                        case "clumpScaleTex":
                        case "clumpRoundnessTex":
                        case "clumpNoiseTex":
                        case "waveScaleTex":
                        case "waveFreqTex":
                        case "rootColorTex":
                        case "tipColorTex":
                        case "strandTex":
                        case "specularTex":
                            listSetting = new CRadHwTextureSetting in parent;
                            ((CRadHwTextureSetting)listSetting).setType(selectedSetting.sid);
                            break;

                        case "stiffnessTexChannel":
                        case "rootStiffnessTexChannel":
                        case "densityTexChannel":
                        case "lengthTexChannel":
                        case "rootWidthTexChannel":
                        case "tipWidthTexChannel":
                        case "clumpScaleTexChannel":
                        case "clumpRoundnessTexChannel":
                        case "clumpNoiseTexChannel":
                        case "waveScaleTexChannel":
                        case "waveFreqTexChannel":
                            listSetting = new CRadHwTextureChannelSetting in parent;
                            break;

                        case "strandBlendMode":
                            listSetting = new CRadHwStrandBlendModeSetting in parent;
                            break;
                    }
                    listSetting.init(selectedSetting, value);

                    parent.selectedListSetting = listSetting;
                    if (listSetting) {
                        parent.PushState(listSetting.getWorkmodeState());
                        return true;
                    } else {
                        return false;
                    }
                    break;

            }
            isValueEditing = true;
            editedValueType = value.type;

            parent.view.listMenuRef.setExtraField("");
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("RADHW_lEditValue"), strValue);
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    event OnLogFurData(action: SInputAction) {
        var defWriter: CModHairworksUiDefinitionWriter;
        var setting: SHWUI_Setting;
        var i: int;

        if (IsPressed(action)) {

            LogChannel('W3FUR', GetLocStringByKeyExt("RADHW_iFurLogStart")
                + " (" + GameTimeToString(theGame.GetGameTime()) + ")");

            LogChannel('W3FUR', "logged component: " + entity.getCurrentComponentListCaption());

            defWriter = new CModHairworksUiDefinitionWriter in this;
            defWriter.init();
            defWriter.logDefinition(hwParams);

            parent.notice(GetLocStringByKeyExt("RADHW_iDefinitionLogged"));
        }
    }
    // ------------------------------------------------------------------------
    protected function updateView() {
        updateCurrentValueCaption();

        // set updated list data and render in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    event OnToggleEmptySettingVisibility(action: SInputAction) {
        if (IsReleased(action)) {
            switch (listProvider.getEmptySettingsFilter()) {
                case EHWUI_ESF_MARK:
                    listProvider.filterEmptySettings(hwParams, EHWUI_ESF_NONE); break;
                default:
                    listProvider.filterEmptySettings(hwParams, EHWUI_ESF_MARK); break;
            }
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    private function updateHwCaption() {
        var newCaption: String;
        var newStatsCaptionId: String;

        newCaption = GetLocStringByKeyExt("RADHW_lListTitle")
                + " <font size=\"8\" color=\"#aaaaaa\">("
                + entity.getCurrentComponentListCaption()
                + ")</font>";

        if (editWetnessVersion) {
            newStatsCaptionId = "RADHW_lListElementsWet";
        } else {
            newStatsCaptionId = "RADHW_lListElementsDry";
        }

        parent.view.title = newCaption;
        parent.view.statsLabel = GetLocStringByKeyExt(newStatsCaptionId);
        // update field if the menu is already open
        parent.view.listMenuRef.setTitle(newCaption);
        parent.view.listMenuRef.setStatsLabel(GetLocStringByKeyExt(newStatsCaptionId));
    }
    // ------------------------------------------------------------------------
    event OnToggleUi(action: SInputAction) {
        if (IsPressed(action)) {
            parent.toggleUi();
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeFurComponent(action: SInputAction) {
        if (action.value != 0) {
            if (action.value < 0) {
                hwParams.setComponent(entity.getPreviousFurCoponent(), editWetnessVersion);
            } else {
                hwParams.setComponent(entity.getNextFurComponent(), editWetnessVersion);
            }
            parent.switchCamTo(RadHwUi_createCamSettingsFor(entity));
            listProvider.refreshFilter(hwParams);
            selectedSetting = listProvider.getMeta(0);
            updateHwCaption();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleDryWetParamSet(action: SInputAction) {
        if (IsPressed(action)) {
            if (entity.hasWetnessComp()) {
                editWetnessVersion = !editWetnessVersion;

                hwParams.setWetnessMaterialSet(editWetnessVersion);
                listProvider.refreshFilter(hwParams);
                listProvider.setSelection(0, true);
                selectedSetting = listProvider.getMeta(0);
                isDirectEditOnly = false;

                updateHwCaption();
                updateView();
                if (editWetnessVersion) {
                    parent.notice(GetLocStringByKeyExt("RADHW_iWetParameterSetActived"));
                } else {
                    parent.notice(GetLocStringByKeyExt("RADHW_iDryParameterSetActived"));
                }
            } else {
                editWetnessVersion = false;
                hwParams.setWetnessMaterialSet(editWetnessVersion);
                parent.error(GetLocStringByKeyExt("RADHW_eNoWetnessComponent"));
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnInteractiveTime(action: SInputAction) {
        if (IsReleased(action)) {
            parent.PushState('RadHw_InteractiveTime');
        }
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        theInput.RegisterListener(this, 'OnToggleUi', 'RADHW_ToggleUi');
        theInput.RegisterListener(this, 'OnFilter', 'RADHW_SetFilter');
        theInput.RegisterListener(this, 'OnResetFilter', 'RADHW_ResetFilter');
        theInput.RegisterListener(this, 'OnCategoryUp', 'RADHW_ListCategoryUp');
        theInput.RegisterListener(this, 'OnCycleSelection', 'RADHW_SelectPrevNext');
        theInput.RegisterListener(this, 'OnChangeValue', 'RADHW_ChangeValue');
        theInput.RegisterListener(this, 'OnChangeValue', 'RADHW_ChangeValueP1');
        theInput.RegisterListener(this, 'OnChangeValue', 'RADHW_ChangeValueP2');
        theInput.RegisterListener(this, 'OnChangeValue', 'RADHW_ChangeValueP3');
        theInput.RegisterListener(this, 'OnChangeValue', 'RADHW_ChangeValueP4');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleSlow');
        theInput.RegisterListener(this, 'OnLogFurData', 'RADHW_LogValues');
        theInput.RegisterListener(parent, 'OnPlayAnim', 'RADHW_PlayAnim');

        theInput.RegisterListener(this, 'OnEditValue', 'RADHW_EditCurrentValue');
        theInput.RegisterListener(this, 'OnInteractiveTime', 'RADHW_AdjustTime');
        theInput.RegisterListener(this, 'OnInteractiveCam', 'RADHW_ToggleInteractiveCam');
        theInput.RegisterListener(this, 'OnChangeFurComponent', 'RADHW_ChangeFurComponent');
        theInput.RegisterListener(this, 'OnToggleDryWetParamSet', 'RADHW_ToggleDryWetParamEditing');
        theInput.RegisterListener(this, 'OnToggleEmptySettingVisibility', 'RADHW_ToggleEmptyVisibility');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADHW_SetFilter');
        theInput.UnregisterListener(this, 'RADHW_ResetFilter');
        theInput.UnregisterListener(this, 'RADHW_ListCategoryUp');
        theInput.UnregisterListener(this, 'RADHW_SelectPrevNext');
        theInput.UnregisterListener(this, 'RADHW_ChangeValue');
        theInput.UnregisterListener(this, 'RADHW_ChangeValueP1');
        theInput.UnregisterListener(this, 'RADHW_ChangeValueP2');
        theInput.UnregisterListener(this, 'RADHW_ChangeValueP3');
        theInput.UnregisterListener(this, 'RADHW_ChangeValueP4');
        theInput.UnregisterListener(this, 'RAD_ToggleFast');
        theInput.UnregisterListener(this, 'RAD_ToggleSlow');
        theInput.UnregisterListener(this, 'RADHW_LogValues');
        theInput.UnregisterListener(this, 'RADHW_PlayAnim');

        theInput.UnregisterListener(this, 'RADHW_EditCurrentValue');
        theInput.UnregisterListener(this, 'RADHW_AdjustTime');
        theInput.UnregisterListener(this, 'RADHW_ToggleInteractiveCam');
        theInput.UnregisterListener(this, 'RADHW_ChangeFurComponent');
        theInput.UnregisterListener(this, 'RADHW_ToggleDryWetParamEditing');
        theInput.UnregisterListener(this, 'RADHW_ToggleEmptyVisibility');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
