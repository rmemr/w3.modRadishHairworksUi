// ----------------------------------------------------------------------------
enum EHWUI_AdjustType {
    EHWUI_SCALAR = 0,
    EHWUI_VEC1 = 1,
    EHWUI_VEC2 = 2,
    EHWUI_VEC3 = 3,
    EHWUI_VEC4 = 4,
}
struct SHWUI_Color {
    var r, g, b, a: Int;
}
// pseudo union
struct SHWUI_Value {
    var type: EHWUI_ValueType;
    var f: Float;
    var b: Bool;
    var i: Int;
    var c: SHWUI_Color;
    var vec: Vector;
    var s: String;
}
enum EHWUI_ValueType {
    EHWUI_FLOAT = 1,
    EHWUI_BOOL = 2,
    EHWUI_INT = 3,
    EHWUI_COLOR = 4,
    EHWUI_VECTOR = 5,
    EHWUI_STRING = 6,
}
// ----------------------------------------------------------------------------
class CModHairworksUiParams {
    // ------------------------------------------------------------------------
    private var fur: CFurComponent;
    private var furMesh: CFurMeshResource;

    private var useMaterialSet: bool;

    private var converter: CModConverter;
    // ------------------------------------------------------------------------
    public function init() {
        converter = (CModConverter)LoadResource("dlc/bootstrap/data/converter.w3mod", true);
    }
    // ------------------------------------------------------------------------
    public function setComponent(comp: CFurComponent, useWetnessSet: bool) {
        this.fur = comp;
        this.furMesh = comp.furMesh;
        this.useMaterialSet = useWetnessSet;
    }
    // ------------------------------------------------------------------------
    public function setWetnessMaterialSet(useWetnessSet: bool) {
        this.useMaterialSet = useWetnessSet;
    }
    // ------------------------------------------------------------------------
    public function usesWetnessMaterialSet() : bool {
        return this.useMaterialSet;
    }
    // ------------------------------------------------------------------------
    private function getPhysicalMaterials() : SFurPhysicalMaterials {
        if (this.useMaterialSet) {
            return this.furMesh.materialSets.physicalMaterials;
        } else {
            return this.furMesh.physicalMaterials;
        }
    }
    // ------------------------------------------------------------------------
    private function getGraphicalMaterials() : SFurGraphicalMaterials {
        if (this.useMaterialSet) {
            return this.furMesh.materialSets.graphicalMaterials;
        } else {
            return this.furMesh.graphicalMaterials;
        }
    }
    // ------------------------------------------------------------------------
    private function valueToHex(v: int) : String {
        switch (v % 16) {
            case 10: return 'a';
            case 11: return 'b';
            case 12: return 'c';
            case 13: return 'd';
            case 14: return 'e';
            case 15: return 'f';
            default: return IntToString(v % 16);
        }
    }
    // ------------------------------------------------------------------------
    private function intToHex(v: int) : String {
        return valueToHex(v / 16) + valueToHex(v);
    }
    // ------------------------------------------------------------------------
    private function colorToHtmlCol(c: SHWUI_Color) : String {
        return "<font color=\"#"
            + intToHex(c.r)
            + intToHex(c.g)
            + intToHex(c.b)
            + "\">&#9751;</font>";
    }
    // ------------------------------------------------------------------------
    private function formatFloatPadded(v: float, precision: int) : String {
        var str: String;

        str = FloatToStringPrec(v, precision);
        if (StrFindFirst(str, ".") < 0) {
            str += ".0";
        }
        if (v < 10) {
            return "  " + str;
        } else if (v < 100) {
            return " " + str;
        } else {
            return str;
        }
    }
    // ------------------------------------------------------------------------
    private function formatValue(v: SHWUI_Value) : String {
        var s: string;
        var cp: SCurveDataEntry;

        s = GetLocStringByKeyExt("RADHW_lGenericValue") + " ";

        switch (v.type) {
            case EHWUI_VECTOR:
                s = GetLocStringByKeyExt("RADHW_lVectorValue");
                s += formatFloatPadded(v.vec.X, 3) + " / "
                    + formatFloatPadded(v.vec.Y, 3) + " / "
                    + formatFloatPadded(v.vec.Z, 3) + " / "
                    + formatFloatPadded(v.vec.W, 3);
                break;

            case EHWUI_COLOR:
                s = GetLocStringByKeyExt("RADHW_lColorValue");
                s += colorToHtmlCol(v.c) + "  "
                    + IntToString(v.c.r) + " / "
                    + IntToString(v.c.g) + " / "
                    + IntToString(v.c.b) + " / "
                    + IntToString(v.c.a);

                break;
            case EHWUI_STRING:
                if (StrLen(v.s) > 50) {
                    s += "..." + StrRight(v.s, 50);
                } else {
                    s += v.s;
                }
                break;

            case EHWUI_FLOAT: s += NoTrailZeros(v.f); break;
            case EHWUI_BOOL: s += v.b; break;
            case EHWUI_INT: s += IntToString(v.i); break;
            default:
                s = "";
        }
        return s;
    }
    // ------------------------------------------------------------------------
    private function toBool(value: bool) : SHWUI_Value {
        return SHWUI_Value(EHWUI_BOOL, , value);
    }
    // ------------------------------------------------------------------------
    private function toFloat(value: float) : SHWUI_Value {
        return SHWUI_Value(EHWUI_FLOAT, value);
    }
    // ------------------------------------------------------------------------
    private function toInt(value: int) : SHWUI_Value {
        return SHWUI_Value(EHWUI_INT, , , value);
    }
    // ------------------------------------------------------------------------
    private function toIntColor(value: Color) : SHWUI_Value {
        return SHWUI_Value(EHWUI_COLOR, , , , SHWUI_Color(
            value.Red,
            value.Green,
            value.Blue,
            value.Alpha));
    }
    // ------------------------------------------------------------------------
    private function toVector(value: Vector) : SHWUI_Value {
        return SHWUI_Value(EHWUI_VECTOR, , , , , value);
    }
    // ------------------------------------------------------------------------
    private function strFromCBitmapTexture(value: CBitmapTexture) : SHWUI_Value {
        return SHWUI_Value(EHWUI_STRING, , , , , , value.GetPath());
    }
    // ------------------------------------------------------------------------
    private function strFromEHairTextureChannel(value: EHairTextureChannel) : SHWUI_Value {
        return SHWUI_Value(EHWUI_STRING, , , , , , value);
    }
    // ------------------------------------------------------------------------
    private function strFromEHairStrandBlendModeType(value: EHairStrandBlendModeType) : SHWUI_Value {
        return SHWUI_Value(EHWUI_STRING, , , , , , value);
    }
    // ------------------------------------------------------------------------
    private function toColor(value: SHWUI_Color) : Color {
        var c: Color;
        c.Red = value.r;
        c.Green = value.g;
        c.Blue = value.b;
        c.Alpha = value.a;
        return c;
    }
    // ------------------------------------------------------------------------
    private function toTextureChannel(channel: String) : EHairTextureChannel {
        switch (channel) {
            case "GREEN":   return GREEN;
            case "BLUE":    return BLUE;
            case "ALPHA":   return ALPHA;
            default:        return RED;
        }
    }
    // ------------------------------------------------------------------------
    private function toTexture(path: String) : CBitmapTexture {
        var newtex: CBitmapTexture;

        if (path != "") {
            newtex = (CBitmapTexture)LoadResource(path, true);

            if (newtex) {
                theGame.GetGuiManager().ShowNotification("using texture " + path);
            } else {
                theGame.GetGuiManager().ShowNotification("texture NOT found: " + path);
                LogChannel('DEBUG', "texture not found: " + path);
            }
        }
        return newtex;
    }
    // ------------------------------------------------------------------------
    private function get_physicalSimulation(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "activated":           return toBool(data.simulation.simulate);
            case "massScale":           return toFloat(data.simulation.massScale);
            case "damping":             return toFloat(data.simulation.damping);
            case "friction":            return toFloat(data.simulation.friction);
            case "backStopRadius":      return toFloat(data.simulation.backStopRadius);
            case "inertiaScale":        return toFloat(data.simulation.inertiaScale);
            case "inertiaLimit":        return toFloat(data.simulation.inertiaLimit);
            case "useCollision":        return toBool(data.simulation.useCollision);
            case "windScaler":          return toFloat(data.simulation.windScaler);
            case "windNoise":           return toFloat(data.simulation.windNoise);
            case "gravityDir":          return toVector(data.simulation.gravityDir);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_physicalVolume(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "density":             return toFloat(data.volume.density);
            case "densityTex":          return strFromCBitmapTexture(data.volume.densityTex);
            case "densityTexChannel":   return strFromEHairTextureChannel(data.volume.densityTexChannel);
            case "usePixelDensity":     return toBool(data.volume.usePixelDensity);
            case "lengthScale":         return toFloat(data.volume.lengthScale);
            case "lengthNoise":         return toFloat(data.volume.lengthNoise);
            case "lengthTex":           return strFromCBitmapTexture(data.volume.lengthTex);
            case "lengthTexChannel":    return strFromEHairTextureChannel(data.volume.lengthTexChannel);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_physicalStrandWidth(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "width":               return toFloat(data.strandWidth.width);
            case "widthRootScale":      return toFloat(data.strandWidth.widthRootScale);
            case "widthTipScale":       return toFloat(data.strandWidth.widthTipScale);
            case "rootWidthTex":        return strFromCBitmapTexture(data.strandWidth.rootWidthTex);
            case "rootWidthTexChannel": return strFromEHairTextureChannel(data.strandWidth.rootWidthTexChannel);
            case "tipWidthTex":         return strFromCBitmapTexture(data.strandWidth.tipWidthTex);
            case "tipWidthTexChannel":  return strFromEHairTextureChannel(data.strandWidth.tipWidthTexChannel);
            case "widthNoise":          return toFloat(data.strandWidth.widthNoise);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_physicalStiffness(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "stiffness":                   return toFloat(data.stiffness.stiffness);
            case "stiffnessStrength":           return toFloat(data.stiffness.stiffnessStrength);
            case "stiffnessTex":                return strFromCBitmapTexture(data.stiffness.stiffnessTex);
            case "stiffnessTexChannel":         return strFromEHairTextureChannel(data.stiffness.stiffnessTexChannel);
            case "interactionStiffness":        return toFloat(data.stiffness.interactionStiffness);
            case "pinStiffness":                return toFloat(data.stiffness.pinStiffness);
            case "rootStiffness":               return toFloat(data.stiffness.rootStiffness);
            case "rootStiffnessTex":            return strFromCBitmapTexture(data.stiffness.rootStiffnessTex);
            case "rootStiffnessTexChannel":     return strFromEHairTextureChannel(data.stiffness.rootStiffnessTexChannel);
            case "stiffnessDamping":            return toFloat(data.stiffness.stiffnessDamping);
            case "tipStiffness":                return toFloat(data.stiffness.tipStiffness);
            case "bendStiffness":               return toFloat(data.stiffness.bendStiffness);
            case "stiffnessBoneEnable":         return toBool(data.stiffness.stiffnessBoneEnable);
            //case "stiffnessBoneIndex":          return toUint32(data.stiffness.stiffnessBoneIndex);
            //case "stiffnessBoneAxis":           return toUint32(data.stiffness.stiffnessBoneAxis);
            case "stiffnessStartDistance":      return toFloat(data.stiffness.stiffnessStartDistance);
            case "stiffnessEndDistance":        return toFloat(data.stiffness.stiffnessEndDistance);
            case "stiffnessBoneCurve":          return toVector(data.stiffness.stiffnessBoneCurve);
            case "stiffnessCurve":              return toVector(data.stiffness.stiffnessCurve);
            case "stiffnessStrengthCurve":      return toVector(data.stiffness.stiffnessStrengthCurve);
            case "stiffnessDampingCurve":       return toVector(data.stiffness.stiffnessDampingCurve);
            case "bendStiffnessCurve":          return toVector(data.stiffness.bendStiffnessCurve);
            case "interactionStiffnessCurve":   return toVector(data.stiffness.interactionStiffnessCurve);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_physicalClumping(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "clumpScale":                  return toFloat(data.clumping.clumpScale);
            case "clumpScaleTex":               return strFromCBitmapTexture(data.clumping.clumpScaleTex);
            case "clumpScaleTexChannel":        return strFromEHairTextureChannel(data.clumping.clumpScaleTexChannel);
            case "clumpRoundness":              return toFloat(data.clumping.clumpRoundness);
            case "clumpRoundnessTex":           return strFromCBitmapTexture(data.clumping.clumpRoundnessTex);
            case "clumpRoundnessTexChannel":    return strFromEHairTextureChannel(data.clumping.clumpRoundnessTexChannel);
            case "clumpNoise":                  return toFloat(data.clumping.clumpNoise);
            case "clumpNoiseTex":               return strFromCBitmapTexture(data.clumping.clumpNoiseTex);
            case "clumpNoiseTexChannel":        return strFromEHairTextureChannel(data.clumping.clumpNoiseTexChannel);
            //case "clumpNumSubclumps":           return toUint32(data.clumping.clumpNumSubclumps);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_physicalWaveness(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurPhysicalMaterials = this.getPhysicalMaterials();

        switch (id) {
            case "waveScale":           return toFloat(data.waveness.waveScale);
            case "waveScaleTex":        return strFromCBitmapTexture(data.waveness.waveScaleTex);
            case "waveScaleTexChannel": return strFromEHairTextureChannel(data.waveness.waveScaleTexChannel);
            case "waveScaleNoise":      return toFloat(data.waveness.waveScaleNoise);
            case "waveFreq":            return toFloat(data.waveness.waveFreq);
            case "waveFreqTex":         return strFromCBitmapTexture(data.waveness.waveFreqTex);
            case "waveFreqTexChannel":  return strFromEHairTextureChannel(data.waveness.waveFreqTexChannel);
            case "waveFreqNoise":       return toFloat(data.waveness.waveFreqNoise);
            case "waveRootStraighten":  return toFloat(data.waveness.waveRootStraighten);
            case "waveStrand":          return toFloat(data.waveness.waveStrand);
            case "waveClump":           return toFloat(data.waveness.waveClump);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialColor(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurGraphicalMaterials = this.getGraphicalMaterials();

        switch (id) {
            case "rootAlphaFalloff":        return toFloat(data.color.rootAlphaFalloff);
            case "rootColor":               return toIntColor(data.color.rootColor);
            case "rootColorTex":            return strFromCBitmapTexture(data.color.rootColorTex);
            case "tipColor":                return toIntColor(data.color.tipColor);
            case "tipColorTex":             return strFromCBitmapTexture(data.color.tipColorTex);
            case "rootTipColorWeight":      return toFloat(data.color.rootTipColorWeight);
            case "rootTipColorFalloff":     return toFloat(data.color.rootTipColorFalloff);
            case "strandTex":               return strFromCBitmapTexture(data.color.strandTex);
            case "strandBlendMode":         return strFromEHairStrandBlendModeType(data.color.strandBlendMode);
            case "strandBlendScale":        return toFloat(data.color.strandBlendScale);
            case "textureBrightness":       return toFloat(data.color.textureBrightness);
            case "ambientEnvScale":         return toFloat(data.color.ambientEnvScale);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialDiffuse(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurGraphicalMaterials = this.getGraphicalMaterials();

        switch (id) {
            case "diffuseBlend":            return toFloat(data.diffuse.diffuseBlend);
            case "diffuseScale":            return toFloat(data.diffuse.diffuseScale);
            case "diffuseHairNormalWeight": return toFloat(data.diffuse.diffuseHairNormalWeight);
            //case "diffuseBoneIndex":        return toFloat(data.diffuse.diffuseBoneIndex); //Uint32
            case "diffuseBoneLocalPos":     return toVector(data.diffuse.diffuseBoneLocalPos);
            case "diffuseNoiseScale":       return toFloat(data.diffuse.diffuseNoiseScale);
            case "diffuseNoiseFreqU":       return toFloat(data.diffuse.diffuseNoiseFreqU);
            case "diffuseNoiseFreqV":       return toFloat(data.diffuse.diffuseNoiseFreqV);
            case "diffuseNoiseGain":        return toFloat(data.diffuse.diffuseNoiseGain);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialSpecular(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurGraphicalMaterials = this.getGraphicalMaterials();

        switch (id) {
            case "specularColor":           return toIntColor(data.specular.specularColor);
            case "specularTex":             return strFromCBitmapTexture(data.specular.specularTex);
            case "specularPrimary":         return toFloat(data.specular.specularPrimary);
            case "specularPowerPrimary":    return toFloat(data.specular.specularPowerPrimary);
            case "specularPrimaryBreakup":  return toFloat(data.specular.specularPrimaryBreakup);
            case "specularSecondary":       return toFloat(data.specular.specularSecondary);
            case "specularPowerSecondary":  return toFloat(data.specular.specularPowerSecondary);
            case "specularSecondaryOffset": return toFloat(data.specular.specularSecondaryOffset);
            case "specularNoiseScale":      return toFloat(data.specular.specularNoiseScale);
            case "specularEnvScale":        return toFloat(data.specular.specularEnvScale);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialGlint(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurGraphicalMaterials = this.getGraphicalMaterials();

        switch (id) {
            case "glintStrength":           return toFloat(data.glint.glintStrength);
            case "glintCount":              return toFloat(data.glint.glintCount);
            case "glintExponent":           return toFloat(data.glint.glintExponent);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialShadow(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurGraphicalMaterials = this.getGraphicalMaterials();

        switch (id) {
            case "shadowSigma":             return toFloat(data.shadow.shadowSigma);
            case "shadowDensityScale":      return toFloat(data.shadow.shadowDensityScale);
            case "castShadows":             return toBool(data.shadow.castShadows);
            case "receiveShadows":          return toBool(data.shadow.receiveShadows);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_materialWeight(id: String) : SHWUI_Value {
        var null: SHWUI_Value;

        switch (id) {
            case "materialWeight":          return toFloat(furMesh.materialWeight);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_levelOfDetail(id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var data: SFurLevelOfDetail = furMesh.levelOfDetail;

        switch (id) {
            case "activated":                   return toBool(data.enableLOD);
            case "useViewfrustrumCulling":      return toBool(data.culling.useViewfrustrumCulling);
            case "useBackfaceCulling":          return toBool(data.culling.useBackfaceCulling);
            case "backfaceCullingThreshold":    return toFloat(data.culling.backfaceCullingThreshold);

            case "enableDistanceLOD":           return toBool(data.distanceLOD.enableDistanceLOD);
            case "distanceLODStart":            return toFloat(data.distanceLOD.distanceLODStart);
            case "distanceLODEnd":              return toFloat(data.distanceLOD.distanceLODEnd);
            case "distanceLODFadeStart":        return toFloat(data.distanceLOD.distanceLODFadeStart);
            case "distanceLODWidth":            return toFloat(data.distanceLOD.distanceLODWidth);
            case "distanceLODDensity":          return toFloat(data.distanceLOD.distanceLODDensity);

            case "enableDetailLOD":             return toBool(data.detailLOD.enableDetailLOD);
            case "detailLODStart":              return toFloat(data.detailLOD.detailLODStart);
            case "detailLODEnd":                return toFloat(data.detailLOD.detailLODEnd);
            case "detailLODWidth":              return toFloat(data.detailLOD.detailLODWidth);
            case "detailLODDensity":            return toFloat(data.detailLOD.detailLODDensity);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_pinContraints(id: String) : SHWUI_Value {
        var null: SHWUI_Value;

        switch (id) {
            case "pins[0].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[0]);
            case "pins[0].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[0]);
            case "pins[1].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[1]);
            case "pins[1].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[1]);
            case "pins[2].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[2]);
            case "pins[2].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[2]);
            case "pins[3].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[3]);
            case "pins[3].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[3]);
            case "pins[4].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[4]);
            case "pins[4].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[4]);
            case "pins[5].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[5]);
            case "pins[5].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[5]);
            case "pins[6].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[6]);
            case "pins[6].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[6]);
            case "pins[7].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[7]);
            case "pins[7].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[7]);
            case "pins[8].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[8]);
            case "pins[8].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[8]);
            case "pins[9].localpos":        return toVector(furMesh.pinConstraintsLocalPosArray[9]);
            case "pins[9].radius":          return toFloat(furMesh.pinConstraintsRadiusArray[9]);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_boneSpheres(id: String) : SHWUI_Value {
        var null: SHWUI_Value;

        switch (id) {
            case "bonesphere[0].localpos":        return toVector(furMesh.boneSphereLocalPosArray[0]);
            case "bonesphere[0].radius":          return toFloat(furMesh.boneSphereRadiusArray[0]);
            case "bonesphere[1].localpos":        return toVector(furMesh.boneSphereLocalPosArray[1]);
            case "bonesphere[1].radius":          return toFloat(furMesh.boneSphereRadiusArray[1]);
            case "bonesphere[2].localpos":        return toVector(furMesh.boneSphereLocalPosArray[2]);
            case "bonesphere[2].radius":          return toFloat(furMesh.boneSphereRadiusArray[2]);
            case "bonesphere[3].localpos":        return toVector(furMesh.boneSphereLocalPosArray[3]);
            case "bonesphere[3].radius":          return toFloat(furMesh.boneSphereRadiusArray[3]);
            case "bonesphere[4].localpos":        return toVector(furMesh.boneSphereLocalPosArray[4]);
            case "bonesphere[4].radius":          return toFloat(furMesh.boneSphereRadiusArray[4]);
            case "bonesphere[5].localpos":        return toVector(furMesh.boneSphereLocalPosArray[5]);
            case "bonesphere[5].radius":          return toFloat(furMesh.boneSphereRadiusArray[5]);
            case "bonesphere[6].localpos":        return toVector(furMesh.boneSphereLocalPosArray[6]);
            case "bonesphere[6].radius":          return toFloat(furMesh.boneSphereRadiusArray[6]);
            case "bonesphere[7].localpos":        return toVector(furMesh.boneSphereLocalPosArray[7]);
            case "bonesphere[7].radius":          return toFloat(furMesh.boneSphereRadiusArray[7]);
            case "bonesphere[8].localpos":        return toVector(furMesh.boneSphereLocalPosArray[8]);
            case "bonesphere[8].radius":          return toFloat(furMesh.boneSphereRadiusArray[8]);
            case "bonesphere[9].localpos":        return toVector(furMesh.boneSphereLocalPosArray[9]);
            case "bonesphere[9].radius":          return toFloat(furMesh.boneSphereRadiusArray[9]);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_meshinfo(id: String) : SHWUI_Value {
        var null: SHWUI_Value;

        switch (id) {
            case "autoHideDistance":        return toFloat(furMesh.autoHideDistance);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    public function getValue(cat1: String, id: String) : SHWUI_Value {
        var null: SHWUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        // top level partitioning
        switch (cat1) {
            case "":                        return get_meshinfo(id);
            case "physical:simulation":     return get_physicalSimulation(id);
            case "physical:volume":         return get_physicalVolume(id);
            case "physical:strand width":   return get_physicalStrandWidth(id);
            case "physical:stiffness":      return get_physicalStiffness(id);
            case "physical:clumping":       return get_physicalClumping(id);
            case "physical:waveness":       return get_physicalWaveness(id);

            case "material:color":          return get_materialColor(id);
            case "material:diffuse":        return get_materialDiffuse(id);
            case "material:specular":       return get_materialSpecular(id);
            case "material:glint":          return get_materialGlint(id);
            case "material:shadow":         return get_materialShadow(id);
            case "material:weight":         return get_materialWeight(id);

            case "level of detail":         return get_levelOfDetail(id);

            case "pins":                    return get_pinContraints(id);
            case "bonespheres":             return get_boneSpheres(id);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    private function set_physicalSimulation(id: String, v: SHWUI_Value) {
        switch (id) {
            case "activated":       furMesh.physicalMaterials.simulation.simulate = v.b; break;
            case "massScale":       furMesh.physicalMaterials.simulation.massScale = v.f; break;
            case "damping":         furMesh.physicalMaterials.simulation.damping = v.f; break;
            case "friction":        furMesh.physicalMaterials.simulation.friction = v.f; break;
            case "backStopRadius":  furMesh.physicalMaterials.simulation.backStopRadius = v.f; break;
            case "inertiaScale":    furMesh.physicalMaterials.simulation.inertiaScale = v.f; break;
            case "inertiaLimit":    furMesh.physicalMaterials.simulation.inertiaLimit = v.f; break;
            case "useCollision":    furMesh.physicalMaterials.simulation.useCollision = v.b; break;
            case "windScaler":      furMesh.physicalMaterials.simulation.windScaler = v.f; break;
            case "windNoise":       furMesh.physicalMaterials.simulation.windNoise = v.f; break;
            case "gravityDir":      furMesh.physicalMaterials.simulation.gravityDir = v.vec; break;

            case "ms.activated":      furMesh.materialSets.physicalMaterials.simulation.simulate = v.b; break;
            case "ms.massScale":      furMesh.materialSets.physicalMaterials.simulation.massScale = v.f; break;
            case "ms.damping":        furMesh.materialSets.physicalMaterials.simulation.damping = v.f; break;
            case "ms.friction":       furMesh.materialSets.physicalMaterials.simulation.friction = v.f; break;
            case "ms.backStopRadius": furMesh.materialSets.physicalMaterials.simulation.backStopRadius = v.f; break;
            case "ms.inertiaScale":   furMesh.materialSets.physicalMaterials.simulation.inertiaScale = v.f; break;
            case "ms.inertiaLimit":   furMesh.materialSets.physicalMaterials.simulation.inertiaLimit = v.f; break;
            case "ms.useCollision":   furMesh.materialSets.physicalMaterials.simulation.useCollision = v.b; break;
            case "ms.windScaler":     furMesh.materialSets.physicalMaterials.simulation.windScaler = v.f; break;
            case "ms.windNoise":      furMesh.materialSets.physicalMaterials.simulation.windNoise = v.f; break;
            case "ms.gravityDir":     furMesh.materialSets.physicalMaterials.simulation.gravityDir = v.vec; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_physicalVolume(id: String, v: SHWUI_Value) {
        switch (id) {
            case "density":             furMesh.physicalMaterials.volume.density = v.f; break;
            case "densityTex":          furMesh.physicalMaterials.volume.densityTex = toTexture(v.s); break;
            case "densityTexChannel":   furMesh.physicalMaterials.volume.densityTexChannel = toTextureChannel(v.s); break;
            case "usePixelDensity":     furMesh.physicalMaterials.volume.usePixelDensity = v.b; break;
            case "lengthScale":         furMesh.physicalMaterials.volume.lengthScale = v.f; break;
            case "lengthNoise":         furMesh.physicalMaterials.volume.lengthNoise = v.f; break;
            case "lengthTex":           furMesh.physicalMaterials.volume.lengthTex = toTexture(v.s); break;
            case "lengthTexChannel":    furMesh.physicalMaterials.volume.lengthTexChannel = toTextureChannel(v.s); break;
            case "ms.density":           furMesh.materialSets.physicalMaterials.volume.density = v.f; break;
            case "ms.densityTex":        furMesh.materialSets.physicalMaterials.volume.densityTex = toTexture(v.s); break;
            case "ms.densityTexChannel": furMesh.materialSets.physicalMaterials.volume.densityTexChannel = toTextureChannel(v.s); break;
            case "ms.usePixelDensity":   furMesh.materialSets.physicalMaterials.volume.usePixelDensity = v.b; break;
            case "ms.lengthScale":       furMesh.materialSets.physicalMaterials.volume.lengthScale = v.f; break;
            case "ms.lengthNoise":       furMesh.materialSets.physicalMaterials.volume.lengthNoise = v.f; break;
            case "ms.lengthTex":         furMesh.materialSets.physicalMaterials.volume.lengthTex = toTexture(v.s); break;
            case "ms.lengthTexChannel":  furMesh.materialSets.physicalMaterials.volume.lengthTexChannel = toTextureChannel(v.s); break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_physicalStrandWidth(id: String, v: SHWUI_Value) {
        switch (id) {
            case "width":               furMesh.physicalMaterials.strandWidth.width = v.f; break;
            case "widthRootScale":      furMesh.physicalMaterials.strandWidth.widthRootScale = v.f; break;
            case "widthTipScale":       furMesh.physicalMaterials.strandWidth.widthTipScale = v.f; break;
            case "rootWidthTex":        furMesh.physicalMaterials.strandWidth.rootWidthTex = toTexture(v.s); break;
            case "rootWidthTexChannel": furMesh.physicalMaterials.strandWidth.rootWidthTexChannel = toTextureChannel(v.s); break;
            case "tipWidthTex":         furMesh.physicalMaterials.strandWidth.tipWidthTex = toTexture(v.s); break;
            case "tipWidthTexChannel":  furMesh.physicalMaterials.strandWidth.tipWidthTexChannel = toTextureChannel(v.s); break;
            case "widthNoise":          furMesh.physicalMaterials.strandWidth.widthNoise = v.f; break;
            case "ms.width":               furMesh.materialSets.physicalMaterials.strandWidth.width = v.f; break;
            case "ms.widthRootScale":      furMesh.materialSets.physicalMaterials.strandWidth.widthRootScale = v.f; break;
            case "ms.widthTipScale":       furMesh.materialSets.physicalMaterials.strandWidth.widthTipScale = v.f; break;
            case "ms.rootWidthTex":        furMesh.materialSets.physicalMaterials.strandWidth.rootWidthTex = toTexture(v.s); break;
            case "ms.rootWidthTexChannel": furMesh.materialSets.physicalMaterials.strandWidth.rootWidthTexChannel = toTextureChannel(v.s); break;
            case "ms.tipWidthTex":         furMesh.materialSets.physicalMaterials.strandWidth.tipWidthTex = toTexture(v.s); break;
            case "ms.tipWidthTexChannel":  furMesh.materialSets.physicalMaterials.strandWidth.tipWidthTexChannel = toTextureChannel(v.s); break;
            case "ms.widthNoise":          furMesh.materialSets.physicalMaterials.strandWidth.widthNoise = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_physicalStiffness(id: String, v: SHWUI_Value) {
        switch (id) {
            case "stiffness":                   furMesh.physicalMaterials.stiffness.stiffness = v.f; break;
            case "stiffnessStrength":           furMesh.physicalMaterials.stiffness.stiffnessStrength = v.f; break;
            case "stiffnessTex":                furMesh.physicalMaterials.stiffness.stiffnessTex = toTexture(v.s); break;
            case "stiffnessTexChannel":         furMesh.physicalMaterials.stiffness.stiffnessTexChannel = toTextureChannel(v.s); break;
            case "interactionStiffness":        furMesh.physicalMaterials.stiffness.interactionStiffness = v.f; break;
            case "pinStiffness":                furMesh.physicalMaterials.stiffness.pinStiffness = v.f; break;
            case "rootStiffness":               furMesh.physicalMaterials.stiffness.rootStiffness = v.f; break;
            case "rootStiffnessTex":            furMesh.physicalMaterials.stiffness.rootStiffnessTex = toTexture(v.s); break;
            case "rootStiffnessTexChannel":     furMesh.physicalMaterials.stiffness.rootStiffnessTexChannel = toTextureChannel(v.s); break;
            case "stiffnessDamping":            furMesh.physicalMaterials.stiffness.stiffnessDamping = v.f; break;
            case "tipStiffness":                furMesh.physicalMaterials.stiffness.tipStiffness = v.f; break;
            case "bendStiffness":               furMesh.physicalMaterials.stiffness.bendStiffness = v.f; break;
            case "stiffnessBoneEnable":         furMesh.physicalMaterials.stiffness.stiffnessBoneEnable = v.b; break;
            //case "stiffnessBoneIndex":          furMesh.physicalMaterials.stiffness.stiffnessBoneIndex = v.?; break;
            //case "stiffnessBoneAxis":           furMesh.physicalMaterials.stiffness.stiffnessBoneAxis = v.?; break;
            case "stiffnessStartDistance":      furMesh.physicalMaterials.stiffness.stiffnessStartDistance = v.f; break;
            case "stiffnessEndDistance":        furMesh.physicalMaterials.stiffness.stiffnessEndDistance = v.f; break;
            case "stiffnessBoneCurve":          furMesh.physicalMaterials.stiffness.stiffnessBoneCurve = v.vec; break;
            case "stiffnessCurve":              furMesh.physicalMaterials.stiffness.stiffnessCurve = v.vec; break;
            case "stiffnessStrengthCurve":      furMesh.physicalMaterials.stiffness.stiffnessStrengthCurve = v.vec; break;
            case "stiffnessDampingCurve":       furMesh.physicalMaterials.stiffness.stiffnessDampingCurve = v.vec; break;
            case "bendStiffnessCurve":          furMesh.physicalMaterials.stiffness.bendStiffnessCurve = v.vec; break;
            case "interactionStiffnessCurve":   furMesh.physicalMaterials.stiffness.interactionStiffnessCurve = v.vec; break;
            case "ms.stiffness":                 furMesh.materialSets.physicalMaterials.stiffness.stiffness = v.f; break;
            case "ms.stiffnessStrength":         furMesh.materialSets.physicalMaterials.stiffness.stiffnessStrength = v.f; break;
            case "ms.stiffnessTex":              furMesh.materialSets.physicalMaterials.stiffness.stiffnessTex = toTexture(v.s); break;
            case "ms.stiffnessTexChannel":       furMesh.materialSets.physicalMaterials.stiffness.stiffnessTexChannel = toTextureChannel(v.s); break;
            case "ms.interactionStiffness":      furMesh.materialSets.physicalMaterials.stiffness.interactionStiffness = v.f; break;
            case "ms.pinStiffness":              furMesh.materialSets.physicalMaterials.stiffness.pinStiffness = v.f; break;
            case "ms.rootStiffness":             furMesh.materialSets.physicalMaterials.stiffness.rootStiffness = v.f; break;
            case "ms.rootStiffnessTex":          furMesh.materialSets.physicalMaterials.stiffness.rootStiffnessTex = toTexture(v.s); break;
            case "ms.rootStiffnessTexChannel":   furMesh.materialSets.physicalMaterials.stiffness.rootStiffnessTexChannel = toTextureChannel(v.s); break;
            case "ms.stiffnessDamping":          furMesh.materialSets.physicalMaterials.stiffness.stiffnessDamping = v.f; break;
            case "ms.tipStiffness":              furMesh.materialSets.physicalMaterials.stiffness.tipStiffness = v.f; break;
            case "ms.bendStiffness":             furMesh.materialSets.physicalMaterials.stiffness.bendStiffness = v.f; break;
            case "ms.stiffnessBoneEnable":       furMesh.materialSets.physicalMaterials.stiffness.stiffnessBoneEnable = v.b; break;
            //case "ms.stiffnessBoneIndex":        furMesh.materialSets.physicalMaterials.stiffness.stiffnessBoneIndex = v.?; break;
            //case "ms.stiffnessBoneAxis":         furMesh.materialSets.physicalMaterials.stiffness.stiffnessBoneAxis = v.?; break;
            case "ms.stiffnessStartDistance":    furMesh.materialSets.physicalMaterials.stiffness.stiffnessStartDistance = v.f; break;
            case "ms.stiffnessEndDistance":      furMesh.materialSets.physicalMaterials.stiffness.stiffnessEndDistance = v.f; break;
            case "ms.stiffnessBoneCurve":        furMesh.materialSets.physicalMaterials.stiffness.stiffnessBoneCurve = v.vec; break;
            case "ms.stiffnessCurve":            furMesh.materialSets.physicalMaterials.stiffness.stiffnessCurve = v.vec; break;
            case "ms.stiffnessStrengthCurve":    furMesh.materialSets.physicalMaterials.stiffness.stiffnessStrengthCurve = v.vec; break;
            case "ms.stiffnessDampingCurve":     furMesh.materialSets.physicalMaterials.stiffness.stiffnessDampingCurve = v.vec; break;
            case "ms.bendStiffnessCurve":        furMesh.materialSets.physicalMaterials.stiffness.bendStiffnessCurve = v.vec; break;
            case "ms.interactionStiffnessCurve": furMesh.materialSets.physicalMaterials.stiffness.interactionStiffnessCurve = v.vec; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_physicalClumping(id: String, v: SHWUI_Value) {
        switch (id) {
            case "clumpScale":                  furMesh.physicalMaterials.clumping.clumpScale = v.f; break;
            case "clumpScaleTex":               furMesh.physicalMaterials.clumping.clumpScaleTex = toTexture(v.s); break;
            case "clumpScaleTexChannel":        furMesh.physicalMaterials.clumping.clumpScaleTexChannel = toTextureChannel(v.s); break;
            case "clumpRoundness":              furMesh.physicalMaterials.clumping.clumpRoundness = v.f; break;
            case "clumpRoundnessTex":           furMesh.physicalMaterials.clumping.clumpRoundnessTex = toTexture(v.s); break;
            case "clumpRoundnessTexChannel":    furMesh.physicalMaterials.clumping.clumpRoundnessTexChannel = toTextureChannel(v.s); break;            case "clumpNoise":                  furMesh.physicalMaterials.clumping.clumpNoise = v.f; break;
            case "clumpNoiseTex":               furMesh.physicalMaterials.clumping.clumpNoiseTex = toTexture(v.s); break;
            case "clumpNoiseTexChannel":        furMesh.physicalMaterials.clumping.clumpNoiseTexChannel = toTextureChannel(v.s); break;
            //case "clumpNumSubclumps":           furMesh.physicalMaterials.clumping.clumpNumSubclumps = v.?; break;
            case "ms.clumpScale":                furMesh.materialSets.physicalMaterials.clumping.clumpScale = v.f; break;
            case "ms.clumpScaleTex":             furMesh.materialSets.physicalMaterials.clumping.clumpScaleTex = toTexture(v.s); break;
            case "ms.clumpScaleTexChannel":      furMesh.materialSets.physicalMaterials.clumping.clumpScaleTexChannel = toTextureChannel(v.s); break;
            case "ms.clumpRoundness":            furMesh.materialSets.physicalMaterials.clumping.clumpRoundness = v.f; break;
            case "ms.clumpRoundnessTex":         furMesh.materialSets.physicalMaterials.clumping.clumpRoundnessTex = toTexture(v.s); break;
            case "ms.clumpRoundnessTexChannel":  furMesh.materialSets.physicalMaterials.clumping.clumpRoundnessTexChannel = toTextureChannel(v.s); break;            case "clumpNoise":                  furMesh.physicalMaterials.clumping.clumpNoise = v.f; break;
            case "ms.clumpNoiseTex":             furMesh.materialSets.physicalMaterials.clumping.clumpNoiseTex = toTexture(v.s); break;
            case "ms.clumpNoiseTexChannel":      furMesh.materialSets.physicalMaterials.clumping.clumpNoiseTexChannel = toTextureChannel(v.s); break;
            //case "ms.clumpNumSubclumps":         furMesh.materialSets.physicalMaterials.clumping.clumpNumSubclumps = v.?; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_physicalWaveness(id: String, v: SHWUI_Value) {
        switch (id) {
            case "waveScale":               furMesh.physicalMaterials.waveness.waveScale = v.f; break;
            case "waveScaleTex":            furMesh.physicalMaterials.waveness.waveScaleTex = toTexture(v.s); break;
            case "waveScaleTexChannel":     furMesh.physicalMaterials.waveness.waveScaleTexChannel = toTextureChannel(v.s); break;
            case "waveScaleNoise":          furMesh.physicalMaterials.waveness.waveScaleNoise = v.f; break;
            case "waveFreq":                furMesh.physicalMaterials.waveness.waveFreq = v.f; break;
            case "waveFreqTex":             furMesh.physicalMaterials.waveness.waveFreqTex = toTexture(v.s); break;
            case "waveFreqTexChannel":      furMesh.physicalMaterials.waveness.waveFreqTexChannel = toTextureChannel(v.s); break;
            case "waveFreqNoise":           furMesh.physicalMaterials.waveness.waveFreqNoise = v.f; break;
            case "waveRootStraighten":      furMesh.physicalMaterials.waveness.waveRootStraighten = v.f; break;
            case "waveStrand":              furMesh.physicalMaterials.waveness.waveStrand = v.f; break;
            case "waveClump":               furMesh.physicalMaterials.waveness.waveClump = v.f; break;
            case "ms.waveScale":            furMesh.materialSets.physicalMaterials.waveness.waveScale = v.f; break;
            case "ms.waveScaleTex":         furMesh.materialSets.physicalMaterials.waveness.waveScaleTex = toTexture(v.s); break;
            case "ms.waveScaleTexChannel":  furMesh.materialSets.physicalMaterials.waveness.waveScaleTexChannel = toTextureChannel(v.s); break;
            case "ms.waveScaleNoise":       furMesh.materialSets.physicalMaterials.waveness.waveScaleNoise = v.f; break;
            case "ms.waveFreq":             furMesh.materialSets.physicalMaterials.waveness.waveFreq = v.f; break;
            case "ms.waveFreqTex":          furMesh.materialSets.physicalMaterials.waveness.waveFreqTex = toTexture(v.s); break;
            case "ms.waveFreqTexChannel":   furMesh.materialSets.physicalMaterials.waveness.waveFreqTexChannel = toTextureChannel(v.s); break;
            case "ms.waveFreqNoise":        furMesh.materialSets.physicalMaterials.waveness.waveFreqNoise = v.f; break;
            case "ms.waveRootStraighten":   furMesh.materialSets.physicalMaterials.waveness.waveRootStraighten = v.f; break;
            case "ms.waveStrand":           furMesh.materialSets.physicalMaterials.waveness.waveStrand = v.f; break;
            case "ms.waveClump":            furMesh.materialSets.physicalMaterials.waveness.waveClump = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialColor(id: String, v: SHWUI_Value) {
        switch (id) {
            case "rootAlphaFalloff":    furMesh.graphicalMaterials.color.rootAlphaFalloff = v.f; break;
            case "rootColor":           furMesh.graphicalMaterials.color.rootColor = toColor(v.c); break;
            case "rootColorTex":        furMesh.graphicalMaterials.color.rootColorTex = toTexture(v.s); break;
            case "tipColor":            furMesh.graphicalMaterials.color.tipColor = toColor(v.c); break;
            case "tipColorTex":         furMesh.graphicalMaterials.color.tipColorTex = toTexture(v.s); break;
            case "rootTipColorWeight":  furMesh.graphicalMaterials.color.rootTipColorWeight = v.f; break;
            case "rootTipColorFalloff": furMesh.graphicalMaterials.color.rootTipColorFalloff = v.f; break;
            case "strandTex":           furMesh.graphicalMaterials.color.strandTex = toTexture(v.s); break;
            case "strandBlendMode":
                switch (v.s) {
                    case "Overwrite": furMesh.graphicalMaterials.color.strandBlendMode = Overwrite; break;
                    case "Multiply":  furMesh.graphicalMaterials.color.strandBlendMode = Multiply; break;
                    case "Add":       furMesh.graphicalMaterials.color.strandBlendMode = Add; break;
                    case "Modulate":  furMesh.graphicalMaterials.color.strandBlendMode = Modulate; break;
                }
                break;
            case "strandBlendScale":    furMesh.graphicalMaterials.color.strandBlendScale = v.f; break;
            case "textureBrightness":   furMesh.graphicalMaterials.color.textureBrightness = v.f; break;
            case "ambientEnvScale":     furMesh.graphicalMaterials.color.ambientEnvScale = v.f; break;
            case "ms.rootAlphaFalloff":    furMesh.materialSets.graphicalMaterials.color.rootAlphaFalloff = v.f; break;
            case "ms.rootColor":           furMesh.materialSets.graphicalMaterials.color.rootColor = toColor(v.c); break;
            case "ms.rootColorTex":        furMesh.materialSets.graphicalMaterials.color.rootColorTex = toTexture(v.s); break;
            case "ms.tipColor":            furMesh.materialSets.graphicalMaterials.color.tipColor = toColor(v.c); break;
            case "ms.tipColorTex":         furMesh.materialSets.graphicalMaterials.color.tipColorTex = toTexture(v.s); break;
            case "ms.rootTipColorWeight":  furMesh.materialSets.graphicalMaterials.color.rootTipColorWeight = v.f; break;
            case "ms.rootTipColorFalloff": furMesh.materialSets.graphicalMaterials.color.rootTipColorFalloff = v.f; break;
            case "ms.strandTex":           furMesh.materialSets.graphicalMaterials.color.strandTex = toTexture(v.s); break;
            case "ms.strandBlendMode":
                switch (v.s) {
                    case "Overwrite": furMesh.materialSets.graphicalMaterials.color.strandBlendMode = Overwrite; break;
                    case "Multiply":  furMesh.materialSets.graphicalMaterials.color.strandBlendMode = Multiply; break;
                    case "Add":       furMesh.materialSets.graphicalMaterials.color.strandBlendMode = Add; break;
                    case "Modulate":  furMesh.materialSets.graphicalMaterials.color.strandBlendMode = Modulate; break;
                }
                break;
            case "ms.strandBlendScale":  furMesh.materialSets.graphicalMaterials.color.strandBlendScale = v.f; break;
            case "ms.textureBrightness": furMesh.materialSets.graphicalMaterials.color.textureBrightness = v.f; break;
            case "ms.ambientEnvScale":   furMesh.materialSets.graphicalMaterials.color.ambientEnvScale = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialDiffuse(id: String, v: SHWUI_Value) {
        switch (id) {
            case "diffuseBlend":            furMesh.graphicalMaterials.diffuse.diffuseBlend = v.f; break;
            case "diffuseScale":            furMesh.graphicalMaterials.diffuse.diffuseScale = v.f; break;
            case "diffuseHairNormalWeight": furMesh.graphicalMaterials.diffuse.diffuseHairNormalWeight = v.f; break;
            //case "diffuseBoneIndex":        furMesh.graphicalMaterials.diffuse.diffuseBoneIndex = v.?; break;
            case "diffuseBoneLocalPos":     furMesh.graphicalMaterials.diffuse.diffuseBoneLocalPos = v.vec; break;
            case "diffuseNoiseScale":       furMesh.graphicalMaterials.diffuse.diffuseNoiseScale = v.f; break;
            case "diffuseNoiseFreqU":       furMesh.graphicalMaterials.diffuse.diffuseNoiseFreqU = v.f; break;
            case "diffuseNoiseFreqV":       furMesh.graphicalMaterials.diffuse.diffuseNoiseFreqV = v.f; break;
            case "diffuseNoiseGain":        furMesh.graphicalMaterials.diffuse.diffuseNoiseGain = v.f; break;
            case "ms.diffuseBlend":            furMesh.materialSets.graphicalMaterials.diffuse.diffuseBlend = v.f; break;
            case "ms.diffuseScale":            furMesh.materialSets.graphicalMaterials.diffuse.diffuseScale = v.f; break;
            case "ms.diffuseHairNormalWeight": furMesh.materialSets.graphicalMaterials.diffuse.diffuseHairNormalWeight = v.f; break;
            //case "ms.diffuseBoneIndex":        furMesh.materialSets.graphicalMaterials.diffuse.diffuseBoneIndex = v.?; break;
            case "ms.diffuseBoneLocalPos":     furMesh.materialSets.graphicalMaterials.diffuse.diffuseBoneLocalPos = v.vec; break;
            case "ms.diffuseNoiseScale":       furMesh.materialSets.graphicalMaterials.diffuse.diffuseNoiseScale = v.f; break;
            case "ms.diffuseNoiseFreqU":       furMesh.materialSets.graphicalMaterials.diffuse.diffuseNoiseFreqU = v.f; break;
            case "ms.diffuseNoiseFreqV":       furMesh.materialSets.graphicalMaterials.diffuse.diffuseNoiseFreqV = v.f; break;
            case "ms.diffuseNoiseGain":        furMesh.materialSets.graphicalMaterials.diffuse.diffuseNoiseGain = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialSpecular(id: String, v: SHWUI_Value) {
        switch (id) {
            case "specularColor":           furMesh.graphicalMaterials.specular.specularColor = toColor(v.c); break;
            case "specularTex":             furMesh.graphicalMaterials.specular.specularTex = toTexture(v.s); break;
            case "specularPrimary":         furMesh.graphicalMaterials.specular.specularPrimary = v.f; break;
            case "specularPowerPrimary":    furMesh.graphicalMaterials.specular.specularPowerPrimary = v.f; break;
            case "specularPrimaryBreakup":  furMesh.graphicalMaterials.specular.specularPrimaryBreakup = v.f; break;
            case "specularSecondary":       furMesh.graphicalMaterials.specular.specularSecondary = v.f; break;
            case "specularPowerSecondary":  furMesh.graphicalMaterials.specular.specularPowerSecondary = v.f; break;
            case "specularSecondaryOffset": furMesh.graphicalMaterials.specular.specularSecondaryOffset = v.f; break;
            case "specularNoiseScale":      furMesh.graphicalMaterials.specular.specularNoiseScale = v.f; break;
            case "specularEnvScale":        furMesh.graphicalMaterials.specular.specularEnvScale = v.f; break;
            case "ms.specularColor":           furMesh.materialSets.graphicalMaterials.specular.specularColor = toColor(v.c); break;
            case "ms.specularTex":             furMesh.materialSets.graphicalMaterials.specular.specularTex = toTexture(v.s); break;
            case "ms.specularPrimary":         furMesh.materialSets.graphicalMaterials.specular.specularPrimary = v.f; break;
            case "ms.specularPowerPrimary":    furMesh.materialSets.graphicalMaterials.specular.specularPowerPrimary = v.f; break;
            case "ms.specularPrimaryBreakup":  furMesh.materialSets.graphicalMaterials.specular.specularPrimaryBreakup = v.f; break;
            case "ms.specularSecondary":       furMesh.materialSets.graphicalMaterials.specular.specularSecondary = v.f; break;
            case "ms.specularPowerSecondary":  furMesh.materialSets.graphicalMaterials.specular.specularPowerSecondary = v.f; break;
            case "ms.specularSecondaryOffset": furMesh.materialSets.graphicalMaterials.specular.specularSecondaryOffset = v.f; break;
            case "ms.specularNoiseScale":      furMesh.materialSets.graphicalMaterials.specular.specularNoiseScale = v.f; break;
            case "ms.specularEnvScale":        furMesh.materialSets.graphicalMaterials.specular.specularEnvScale = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialGlint(id: String, v: SHWUI_Value) {
        switch (id) {
            case "glintStrength":           furMesh.graphicalMaterials.glint.glintStrength = v.f; break;
            case "glintCount":              furMesh.graphicalMaterials.glint.glintCount = v.f; break;
            case "glintExponent":           furMesh.graphicalMaterials.glint.glintExponent = v.f; break;
            case "ms.glintStrength":        furMesh.materialSets.graphicalMaterials.glint.glintStrength = v.f; break;
            case "ms.glintCount":           furMesh.materialSets.graphicalMaterials.glint.glintCount = v.f; break;
            case "ms.glintExponent":        furMesh.materialSets.graphicalMaterials.glint.glintExponent = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialShadow(id: String, v: SHWUI_Value) {
        switch (id) {
            case "shadowSigma":             furMesh.graphicalMaterials.shadow.shadowSigma = v.f; break;
            case "shadowDensityScale":      furMesh.graphicalMaterials.shadow.shadowDensityScale = v.f; break;
            case "castShadows":             furMesh.graphicalMaterials.shadow.castShadows = v.b; break;
            case "receiveShadows":          furMesh.graphicalMaterials.shadow.receiveShadows = v.b; break;
            case "ms.shadowSigma":          furMesh.materialSets.graphicalMaterials.shadow.shadowSigma = v.f; break;
            case "ms.shadowDensityScale":   furMesh.materialSets.graphicalMaterials.shadow.shadowDensityScale = v.f; break;
            case "ms.castShadows":          furMesh.materialSets.graphicalMaterials.shadow.castShadows = v.b; break;
            case "ms.receiveShadows":       furMesh.materialSets.graphicalMaterials.shadow.receiveShadows = v.b; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_materialWeight(id: String, v: SHWUI_Value) {
        switch (id) {
            case "materialWeight":          furMesh.materialWeight = v.f; break;
            case "ms.materialWeight":       furMesh.materialWeight = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_levelOfDetail(id: String, v: SHWUI_Value) {
        switch (id) {
            case "activated":                   furMesh.levelOfDetail.enableLOD = v.b; break;
            case "useViewfrustrumCulling":      furMesh.levelOfDetail.culling.useViewfrustrumCulling = v.b; break;
            case "useBackfaceCulling":          furMesh.levelOfDetail.culling.useBackfaceCulling = v.b; break;
            case "backfaceCullingThreshold":    furMesh.levelOfDetail.culling.backfaceCullingThreshold = v.f; break;
            case "enableDistanceLOD":           furMesh.levelOfDetail.distanceLOD.enableDistanceLOD = v.b; break;
            case "distanceLODStart":            furMesh.levelOfDetail.distanceLOD.distanceLODStart = v.f; break;
            case "distanceLODEnd":              furMesh.levelOfDetail.distanceLOD.distanceLODEnd = v.f; break;
            case "distanceLODFadeStart":        furMesh.levelOfDetail.distanceLOD.distanceLODFadeStart = v.f; break;
            case "distanceLODWidth":            furMesh.levelOfDetail.distanceLOD.distanceLODWidth = v.f; break;
            case "distanceLODDensity":          furMesh.levelOfDetail.distanceLOD.distanceLODDensity = v.f; break;
            case "enableDetailLOD":             furMesh.levelOfDetail.detailLOD.enableDetailLOD = v.b; break;
            case "detailLODStart":              furMesh.levelOfDetail.detailLOD.detailLODStart = v.f; break;
            case "detailLODEnd":                furMesh.levelOfDetail.detailLOD.detailLODEnd = v.f; break;
            case "detailLODWidth":              furMesh.levelOfDetail.detailLOD.detailLODWidth = v.f; break;
            case "detailLODDensity":            furMesh.levelOfDetail.detailLOD.detailLODDensity = v.f; break;
            //case "priority":                  furMesh.levelOfDetail.priority = v.?; break;
        }
    }
    // ------------------------------------------------------------------------
    public function setValue(cat1: string, id: string, v: SHWUI_Value) {
        // toggle to wetness settings
        if (cat1 != "level of detail" && this.useMaterialSet) { id = "ms." + id; }

        // top level partitioning
        switch (cat1) {
            case "physical:simulation":     set_physicalSimulation(id, v); break;
            case "physical:volume":         set_physicalVolume(id, v); break;
            case "physical:strand width":   set_physicalStrandWidth(id, v); break;
            case "physical:stiffness":      set_physicalStiffness(id, v); break;
            case "physical:clumping":       set_physicalClumping(id, v); break;
            case "physical:waveness":       set_physicalWaveness(id, v); break;

            case "material:color":          set_materialColor(id, v); break;
            case "material:diffuse":        set_materialDiffuse(id, v); break;
            case "material:specular":       set_materialSpecular(id, v); break;
            case "material:glint":          set_materialGlint(id, v); break;
            case "material:shadow":         set_materialShadow(id, v); break;
            case "material:weight":         set_materialWeight(id, v); break;

            case "level of detail":         set_levelOfDetail(id, v);
        }
    }
    // ------------------------------------------------------------------------
    private function adjust(
        v: SHWUI_Value, delta: float, type: EHWUI_AdjustType,
        min: float, max: float) : SHWUI_Value
    {
        if (min == max) {
            min = -100000.0;
            max = 100000.0;
        }
        switch (v.type) {
            case EHWUI_FLOAT:    v.f = ClampF(v.f + delta, min, max); break;
            case EHWUI_BOOL:     v.b = delta > 0; break;
            case EHWUI_INT:      v.i = RoundF(ClampF(v.i + delta, min, max)); break;
            case EHWUI_COLOR:
                switch (type) {
                    case EHWUI_VEC1: v.c.r = RoundF(ClampF(v.c.r + delta, min, max)); break;
                    case EHWUI_VEC2: v.c.g = RoundF(ClampF(v.c.g + delta, min, max)); break;
                    case EHWUI_VEC3: v.c.b = RoundF(ClampF(v.c.b + delta, min, max)); break;
                    case EHWUI_VEC4: v.c.a = RoundF(ClampF(v.c.a + delta, min, max)); break;
                }
                break;
            case EHWUI_VECTOR:
                switch (type) {
                    case EHWUI_VEC1: v.vec.X = ClampF(v.vec.X + delta, min, max); break;
                    case EHWUI_VEC2: v.vec.Y = ClampF(v.vec.Y + delta, min, max); break;
                    case EHWUI_VEC3: v.vec.Z = ClampF(v.vec.Z + delta, min, max); break;
                    case EHWUI_VEC4: v.vec.W = ClampF(v.vec.W + delta, min, max); break;
                }
                break;
        }
        return v;
    }
    // ------------------------------------------------------------------------
    public function adjustValue(
        cat1: String, id: String, delta: float, type: EHWUI_AdjustType,
        min: float, max: float) : SHWUI_Value
    {
        var v: SHWUI_Value;
        v = adjust(getValue(cat1, id), delta, type, min, max);
        setValue(cat1, id, v);
        return v;
    }
    // ------------------------------------------------------------------------
    public function getCurrentValue(cat1: String, id: String) : SHWUI_Value {
        var s: SHWUI_Value;
        var d: SHWUI_Value;

        s = getValue(cat1, id);

        // copy the relevant data
        d.type = s.type;
        switch (s.type) {
            case EHWUI_VECTOR:   d.vec = s.vec; break;
            case EHWUI_COLOR:    d.c = s.c; break;
            case EHWUI_FLOAT:    d.f = s.f; break;
            case EHWUI_BOOL:     d.b = s.b; break;
            case EHWUI_INT:      d.i = s.i; break;
            case EHWUI_STRING:   d.s = s.s; break;
        }
        return d;
    }
    // ------------------------------------------------------------------------
    public function setCurrentValue(
        cat1: String, id: String, newValue: SHWUI_Value)
    {
        var d: SHWUI_Value;
        var cp, newCp: SCurveDataEntry;

        d = getValue(cat1, id);

        // copy the relevant data
        switch (d.type) {
            case EHWUI_VECTOR:   d.vec = newValue.vec; break;
            case EHWUI_COLOR:    d.c = newValue.c; break;
            case EHWUI_FLOAT:    d.f = newValue.f; break;
            case EHWUI_BOOL:     d.b = newValue.b; break;
            case EHWUI_INT:      d.i = newValue.i; break;
            case EHWUI_STRING:   d.s = newValue.s; break;
        }
        setValue(cat1, id, d);
    }
    // ------------------------------------------------------------------------
    public function getFormattedValue(cat1: String, id: String) : String {
        return formatValue(getValue(cat1, id));
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
