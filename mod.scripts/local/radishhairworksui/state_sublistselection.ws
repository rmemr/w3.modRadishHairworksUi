// ----------------------------------------------------------------------------
state RadHw_TextureSelection in CRadishHairworksEditor extends RadHw_GenericListSelection {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        editedSetting = parent.selectedListSetting;
        super.setupLabels("Texture", editedSetting.getCaption());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function setListSelection() {
        listProvider.setOrAddSelection(editedSetting.getValueId(), true);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadHw_TextureChannelSelection in CRadishHairworksEditor extends RadHw_GenericListSelection {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        editedSetting = parent.selectedListSetting;
        super.setupLabels("TextureChannel", editedSetting.getCaption());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadHw_BlendModeSelection in CRadishHairworksEditor extends RadHw_GenericListSelection {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        editedSetting = parent.selectedListSetting;
        super.setupLabels("BlendMode", editedSetting.getCaption());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
