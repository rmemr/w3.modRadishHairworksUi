// ----------------------------------------------------------------------------
abstract state RadHw_GenericListSelection in CRadishHairworksEditor {
    // ------------------------------------------------------------------------
    protected var editedSetting: CRadHwUiListSetting;
    protected var selectedValue: String;

    protected var listProvider: CRadHwGenericSettingList;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        listProvider = parent.getListProvider(editedSetting.getValueListId());
        this.setListSelection();

        theInput.StoreContext(editedSetting.getInputContext());

        registerListeners();

        parent.showUi(true);
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    protected function setListSelection() {
        listProvider.setSelection(editedSetting.getValueId(), true);
    }
    // ------------------------------------------------------------------------
    protected function setupLabels(type: String, itemName: String) {
        parent.view.title = GetLocStringByKeyExt("RADHW_" + type + "Title") + " " + itemName;
        parent.view.statsLabel = GetLocStringByKeyExt("RADHW_" + type + "ListElements");

        // update fields if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);
        parent.view.listMenuRef.setExtraField("");
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        unregisterListeners();
        theInput.RestoreContext(editedSetting.getInputContext(), true);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_SelectPrevNext'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_PlayAnim'));
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ToggleInteractiveCam', 'RAD_ToggleInteractiveCam', IK_LControl));
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        // important that this is on release!
        if (IsReleased(action)) {
            parent.backToPreviousState(action);
        }
    }
    // ------------------------------------------------------------------------
    event OnInteractiveCam(action: SInputAction) {
        if (IsReleased(action)) {
            parent.showUi(false);
            parent.PushState('RadHw_InteractiveCamera');
        }
    }
    // ------------------------------------------------------------------------
    protected function onListSelection(selectedId: String);
    // ------------------------------------------------------------------------
    event OnSelected(selectedId: String) {
        if (listProvider.setSelection(selectedId, true)) {
            editedSetting.setValueId(selectedId);
            parent.hwParams.setCurrentValue(
                editedSetting.getCategory(),
                editedSetting.getId(),
                editedSetting.getValue());
            parent.furEntities.getCurrent().refresh();
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnCycleSelection(action: SInputAction) {
        var prev: bool;
        var id: String;

        if (action.value != 0) {
            prev = action.value < 0;

            if (prev) {
                id = listProvider.getPreviousId();
            } else {
                id = listProvider.getNextId();
            }
            OnSelected(id);
        }
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {
        updateCurrentValueCaption();
        updateView();
    }
    // ------------------------------------------------------------------------
    private function updateCurrentValueCaption() {
        parent.view.listMenuRef.setExtraField(
                parent.hwParams.getFormattedValue(
                    editedSetting.getCategory(), editedSetting.getId()
                ), "#aaaaaa");
    }
    // ------------------------------------------------------------------------
    protected function updateView() {
        // provide info to override stats info in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            // list may contain category entries which do not count as items!
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');
        theInput.RegisterListener(this, 'OnCycleSelection', 'RADHW_SelectPrevNext');
        theInput.RegisterListener(this, 'OnInteractiveCam', 'RADHW_ToggleInteractiveCam');
        theInput.RegisterListener(parent, 'OnPlayAnim', 'RADHW_PlayAnim');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RAD_Back');
        theInput.UnregisterListener(this, 'RADHW_SelectPrevNext');
        theInput.UnregisterListener(this, 'RADHW_ToggleInteractiveCam');
        theInput.UnregisterListener(this, 'RADHW_PlayAnim');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
