// ----------------------------------------------------------------------------
abstract class CRadHwUiListSetting {
    // ------------------------------------------------------------------------
    protected var valueListId: String;
    protected var workModeState: CName;
    protected var inputContext: CName;  default inputContext = 'MOD_RadHw_SublistSelection';
    // ------------------------------------------------------------------------
    protected var setting: SHWUI_Setting;
    protected var value: SHWUI_Value;
    // ------------------------------------------------------------------------
    public function init(setting: SHWUI_Setting, value: SHWUI_Value) {
        this.setting = setting;
        this.value = value;
    }
    // ------------------------------------------------------------------------
    public function getWorkmodeState() : CName {
        return workModeState;
    }
    // ------------------------------------------------------------------------
    public function getInputContext() : CName {
        return inputContext;
    }
    // ------------------------------------------------------------------------
    public function getValueListId() : String {
        return valueListId;
    }
    // ------------------------------------------------------------------------
    public function getValueId() : String {
        return this.value.s;
    }
    // ------------------------------------------------------------------------
    public function setValueId(valueId: String) {
        this.value.s = valueId;
    }
    // ------------------------------------------------------------------------
    public function getValue() : SHWUI_Value {
        return this.value;
    }
    // ------------------------------------------------------------------------
    public function getCategory() : String {
        return setting.cat1;
    }
    // ------------------------------------------------------------------------
    public function getId() : String {
        return setting.sid;
    }
    // ------------------------------------------------------------------------
    public function getCaption() : String {
        return setting.caption;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadHwTextureSetting extends CRadHwUiListSetting {
    default workModeState = 'RadHw_TextureSelection';
    default valueListId = "TextureSetting";
    // ------------------------------------------------------------------------
    public function setType(type: String) {
        switch (type) {
            case "stiffnessTex":
            case "rootStiffnessTex":
                valueListId = "StiffnessTextures";
                break;

            case "densityTex":
            case "lengthTex":
                valueListId = "VolumeTextures";
                break;

            case "rootWidthTex":
            case "tipWidthTex":
                valueListId = "WidthTextures";
                break;

            case "clumpScaleTex":
            case "clumpRoundnessTex":
            case "clumpNoiseTex":
                valueListId = "ClumpTextures";
                break;

            case "waveScaleTex":
            case "waveFreqTex":
                valueListId = "WaveTextures";
                break;
            case "rootColorTex":
            case "tipColorTex":
            case "specularTex":
                valueListId = "ColorTextures";
                break;
            case "strandTex":
                valueListId = "StrandTextures";
                break;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadHwTextureChannelSetting extends CRadHwUiListSetting {
    default workModeState = 'RadHw_TextureChannelSelection';
    default valueListId = "TextureChannel";
}
// ----------------------------------------------------------------------------
class CRadHwStrandBlendModeSetting extends CRadHwUiListSetting {
    default workModeState = 'RadHw_BlendModeSelection';
    default valueListId = "StrandBlendMode";
}
// ----------------------------------------------------------------------------
