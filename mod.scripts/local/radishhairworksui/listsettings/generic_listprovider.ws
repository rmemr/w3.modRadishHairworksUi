// ----------------------------------------------------------------------------
class CRadHwGenericSettingList extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    public function preselect(optional openCategories: bool) {
        if (items.Size() > 0) {
            selectedId = items[0].id;
            if (openCategories) {
                selectedCat1 = items[0].cat1;
                selectedCat2 = items[0].cat2;
                selectedCat3 = items[0].cat3;
            }
        }
    }
    // ------------------------------------------------------------------------
    private function shorten(input: String) : String {
        // add unknown setting (e.g. current texture setting value)
        if (StrLen(input) > 32) {
            return "..." + StrRight(input, 32);
        } else {
            return input;
        }
    }
    // ------------------------------------------------------------------------
    public function setOrAddSelection(id: String, optional openCategories: bool) : bool {
        var i: int;

        for (i = 0; i < items.Size(); i += 1) {
            if (items[i].id == id) {
                return setSelection(id, openCategories);
            }
        }

        items.PushBack(SModUiCategorizedListItem(id, shorten(id)));
        return setSelection(id, openCategories);
    }
    // ------------------------------------------------------------------------
    public function getSelectedId() : String {
        return selectedId;
    }
    // ------------------------------------------------------------------------
    public function addCustomEntries(entries: array<String>) {
        var topCat: String;
        var i: int;

        for (i = 0; i < entries.Size(); i += 1) {
            items.PushBack(SModUiCategorizedListItem(entries[i], shorten(entries[i])));
        }
    }
    // ------------------------------------------------------------------------
    public function addEntries(entries: array<SModUiCategorizedListItem>) {
        var i: int;

        for (i = 0; i < entries.Size(); i += 1) {
            items.PushBack(entries[i]);
        }
    }
    // ------------------------------------------------------------------------
    public function addEmptyEntry() {
        items.PushBack(SModUiCategorizedListItem("", GetLocStringByKeyExt("RADHW_NoTextureValue")));
    }
    // ------------------------------------------------------------------------
    public function clear() {
        items.Clear();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadHwSettingListProvider {
    // ------------------------------------------------------------------------
    private var loadedLists: array<String>;
    private var providerList: array<CRadHwGenericSettingList>;
    // ------------------------------------------------------------------------
    public function getListProvider(listId: String) : CRadHwGenericSettingList {
        var list: CRadHwGenericSettingList;

        if (!loadedLists.Contains(listId)) {
            list = new CRadHwGenericSettingList in this;

            list.clear();

            switch (listId) {
                case "TextureChannel":
                case "StrandBlendMode":
                    list.addEntries(getSettingListEntries(listId));
                    break;
                default:
                    list.addEmptyEntry();
                    list.addCustomEntries(RADHW_getCustomTextureListEntries(listId));
            }

            providerList.PushBack(list);
            loadedLists.PushBack(listId);
        }

        return providerList[loadedLists.FindFirst(listId)];
    }
    // ------------------------------------------------------------------------
    protected function getSettingListEntries(listId: String)
        : array<SModUiCategorizedListItem>
    {
        var entries: array<SModUiCategorizedListItem>;

        switch (listId) {
            case "TextureChannel":
                entries.PushBack(SModUiCategorizedListItem("RED", "red"));
                entries.PushBack(SModUiCategorizedListItem("GREEN", "green"));
                entries.PushBack(SModUiCategorizedListItem("BLUE", "blue"));
                entries.PushBack(SModUiCategorizedListItem("ALPHA", "alpha"));
                break;

            case "StrandBlendMode":
                entries.PushBack(SModUiCategorizedListItem("Overwrite", "overwrite"));
                entries.PushBack(SModUiCategorizedListItem("Multiply", "multiply"));
                entries.PushBack(SModUiCategorizedListItem("Add", "add"));
                entries.PushBack(SModUiCategorizedListItem("Modulate", "modulate"));
                break;
        }
        return entries;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------