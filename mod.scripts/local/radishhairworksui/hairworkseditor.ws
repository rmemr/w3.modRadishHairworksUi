// ----------------------------------------------------------------------------
// callback for generic ui list which will call example mod back
class CModHairworksUiListCallback extends IModUiEditableListCallback {
    public var callback: CRadishHairworksEditor;

    public function OnConfigUi() {
        var topMenuConf: SModUiTopMenuConfig;

        // overwrite default config to save screenspace and use extra field
        listMenuRef.setMenuConfig(SModUiMenuConfig(67.0, 95.0, 22));
        // patch top menu config
        topMenuConf = listMenuRef.getTopMenuConfig();
        topMenuConf.y -= 20.0;
        topMenuConf.height = 22.7;

        topMenuConf.titleField.x = 1;
        topMenuConf.titleField.y -= 20;

        topMenuConf.statsField.x = 3;
        topMenuConf.statsField.y -= 20;

        topMenuConf.editField.x = 3;
        topMenuConf.editField.y -= 20;

        topMenuConf.extraField.x = 4;
        topMenuConf.extraField.y = 152;

        topMenuConf.titleField.fontSize = 17;
        topMenuConf.statsField.fontSize = 12;
        topMenuConf.editField.fontSize = 11;
        topMenuConf.extraField.fontSize = 9;

        listMenuRef.setTopMenuConfig(topMenuConf);
    }

    public function OnOpened() { callback.OnUpdateView(); }

    public function OnInputEnd(inputString: String) { callback.OnInputEnd(inputString); }

    public function OnInputCancel() { callback.OnInputCancel(); }

    public function OnClosed() {
        var null: CModUiEditableListView;
        delete this.listMenuRef;
        this.listMenuRef = null;
        callback.OnClosedView();
    }

    public function OnSelected(optionName: String) { callback.OnSelected(optionName); }
}
// ----------------------------------------------------------------------------
state RadHw_Sleeping in CRadishHairworksEditor {}
// ----------------------------------------------------------------------------
statemachine class CRadishHairworksEditor {
    protected var workContext: CName; default workContext = 'MOD_HairworksUi';
    // ------------------------------------------------------------------------
    protected var view: CModHairworksUiListCallback;
    protected var listProvider: CModHairworksUiList;

    protected var hwParams: CModHairworksUiParams;

    protected var furEntities: CModHairworksUiFurEntityPool;
    // ------------------------------------------------------------------------
    protected var genericListProvider: CRadHwSettingListProvider;
    protected var selectedListSetting: CRadHwUiListSetting;
    // ------------------------------------------------------------------------
    // UI stuff
    private var confirmPopup: CModUiActionConfirmation;
    private var quitConfirmCallback: CRadHwPopupCallback;
    // ------------------------------------------------------------------------
    private var globalHotKeyMode: bool;
    // ------------------------------------------------------------------------
    private var cameraTracker: CRadishTracker;
    // ------------------------------------------------------------------------
    protected var theCam: CRadishStaticCamera;
    // ------------------------------------------------------------------------
    private var configManager: CRadishHairworksUiConfigManager;
    // ------------------------------------------------------------------------
    // flag indicating to pop state *after* currently open view is closed
    private var popStateAfterViewClose: bool;
    // ------------------------------------------------------------------------
    public function init(
        log: CModLogger, configManager: CRadishHairworksUiConfigManager, quitConfirmCallback: CRadHwPopupCallback)
    {
        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("RADHW_Started"));

        this.configManager = configManager;
        this.quitConfirmCallback = quitConfirmCallback;

        // prepare view callback wiring and set labels
        view = new CModHairworksUiListCallback in this;
        view.callback = this;
        view.title = GetLocStringByKeyExt("RADHW_lListTitle");
        view.statsLabel = GetLocStringByKeyExt("RADHW_lListElements");

        // load list of settings
        listProvider = new CModHairworksUiList in this;
        listProvider.initList();

        // init generic settinglist values
        genericListProvider = new CRadHwSettingListProvider in this;

        hwParams = new CModHairworksUiParams in this;
        hwParams.init();

        furEntities = new CModHairworksUiFurEntityPool in this;

        this.registerListeners();
        GotoState('RadHw_Sleeping');
    }
    // ------------------------------------------------------------------------
    public function activate() : bool {
        var count: int;

        // scan for furry entities, first will be "selected"
        count = furEntities.rescan();

        theGame.GetGuiManager().ShowNotification(
            GetLocStringByKeyExt("RADHW_iFoundFurries") + " " + IntToString(count));

        if (count == 0) {
            return false;
        }

        this.theCam = this.createStaticCamera();
        this.cameraTracker = this.createCameraTracker();

        // tracker must be attached to cam because cam position adjusts (invisible)
        // player pos on cam movement and visibility trigger position must be synced
        // with player pos
        theCam.setTracker(cameraTracker);
        theCam.activate();
        theCam.setSettings(RadHwUi_createCamSettingsFor(furEntities.getCurrent()));
        theCam.setFov(25);
        theCam.switchTo();

        this.registerListeners();

        theInput.StoreContext(workContext);

        PushState('RadHw_Active');

        return true;
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        var i, s: int;

        PopState();
        theInput.RestoreContext(workContext, true);

        this.unregisterListeners();
        this.showUi(false);

        this.cameraTracker.stop();
        this.cameraTracker.Destroy();
        theCam.deactivate();
        theCam.Destroy();

        PushState('RadHw_Sleeping');
    }
    // ------------------------------------------------------------------------
    protected function getListProvider(listId: String) : CRadHwGenericSettingList {
        return genericListProvider.getListProvider(listId);
    }
    // ------------------------------------------------------------------------
    public function showUi(showUi: bool) {
        if (showUi) {
            if (!view.listMenuRef) {
                theGame.RequestMenu('ListView', view);
            }
        } else {
            if (view.listMenuRef) {
                view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
    public function switchCamTo(placement: SRadishPlacement) {
        theCam.setSettings(placement);
        theCam.switchTo();
    }
    // ------------------------------------------------------------------------
    private function createStaticCamera() : CRadishStaticCamera {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\static_camera.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishStaticCamera)entity;
    }
    // ------------------------------------------------------------------------
    private function createCameraTracker() : CRadishTracker {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_tracker.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishTracker)entity;
    }
    // ------------------------------------------------------------------------
    protected function getCamTracker() : CRadishTracker {
        return this.cameraTracker;
    }
    // ------------------------------------------------------------------------
    protected function getCamPlacement() : SRadishPlacement {
        return theCam.getSettings();
    }
    // ------------------------------------------------------------------------
    protected function getConfig() : IRadishConfigManager {
        return this.configManager;
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        configManager.setLastCamPosition(theCam.getSettings());

        //.logDefinition();

        unregisterListeners();
        // to prevent problems with wrongly restored state use this hardcoded
        // "safe" value
        theInput.SetContext('Exploration');

        deactivate();
        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("RADHW_Stopped"));
    }
    // ------------------------------------------------------------------------
    public function quitRequest() {
        var msgTitle: String;
        var msgText: String;

        if (confirmPopup) { delete confirmPopup; }

        confirmPopup = new CModUiActionConfirmation in this;
        msgTitle = "RADHW_tQuitConfirm";
        msgText = "RADHW_mQuitConfirm";

        confirmPopup.open(quitConfirmCallback,
            GetLocStringByKeyExt(msgTitle),
            GetLocStringByKeyExt(msgText), "quit");
    }
    // ------------------------------------------------------------------------
    event OnQuitRequest(action: SInputAction) {
        if (IsPressed(action)) {
            quitRequest();
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADHW_ShowHelp'));
    }
    // ------------------------------------------------------------------------
    event OnHelpMePrettyPlease(action: SInputAction) {
        var helpPopup: CModUiHotkeyHelp;
        var titleKey: String;
        var introText: String;
        var hotkeyList: array<SModUiHotkeyHelp>;

        if (IsPressed(action)) {
            helpPopup = new CModUiHotkeyHelp in this;

            titleKey = "RADHW_tHelpHotkey";
            introText = "<p>" + GetLocStringByKeyExt("RADHW_mGeneralHelp") + "</p>";

            this.OnHotkeyHelp(hotkeyList);

            helpPopup.open(titleKey, introText, hotkeyList);
        }
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {}
    event OnInputEnd(inputString: String) {}
    event OnInputCancel() {}
    event OnSelected(listItemId: String) {}
    // ------------------------------------------------------------------------
    event OnPlayAnim(action: SInputAction) {
        if (IsPressed(action)) {
            if (!furEntities.getCurrent().playExampleAnimation()) {
                this.error(GetLocStringByKeyExt("RADHW_ePreviewAnimFailed"));
            }

        }
    }
    // ------------------------------------------------------------------------
    event OnClosedView() {
        var null: SInputAction;
        if (popStateAfterViewClose) {
            popStateAfterViewClose = false;
            PopState();
        }
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        theGame.GetGuiManager().ShowNotification(msg);
    }
    // ------------------------------------------------------------------------
    protected function error(errMsg: String) {
        theGame.GetGuiManager().ShowNotification(errMsg);
    }
    // ------------------------------------------------------------------------
    public function isUiShown() : bool {
        return view.listMenuRef;
    }
    // ------------------------------------------------------------------------
    protected function toggleUi() {
        showUi(!isUiShown());
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        var null: CModUiEditableListView;

        if (IsReleased(action)) {
            if (isUiShown()) {
                // defer restoring previous state to enable clean close of listview
                // otherwise (previous) state tries to open view on OnEnterState but
                // it gets immediately closed again
                popStateAfterViewClose = true;
                // since back is bound to ESCAPE which automatically closes an
                // open menu view the view must no be closed *again*
                delete view.listMenuRef;
                view.listMenuRef = null;
            } else {
                PopState();
            }
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        // -- generic hotkeys
        theInput.RegisterListener(this, 'OnHelpMePrettyPlease', 'RADHW_ShowHelp');
        theInput.RegisterListener(this, 'OnQuitRequest', 'RADHW_Quit');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        // -- generic hotkeys
        theInput.UnregisterListener(this, 'RADHW_ShowHelp');
        theInput.UnregisterListener(this, 'RADHW_Quit');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
