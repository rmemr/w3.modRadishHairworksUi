// ----------------------------------------------------------------------------
state RadHw_InteractiveCamera in CRadishHairworksEditor extends Rad_InteractiveRotatingCamera
{
    default workContext = 'MOD_RadHw_ModeInteractiveCam';
    // ------------------------------------------------------------------------
    private var entity: CModHairworksUiFurEntity;
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        parent.PopState();
    }
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));

        entity = parent.furEntities.getCurrent();

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        // interactive cam MUST be stopped before changing to static cam!
        theCam.stopInteractiveMode();

        // reactivate the static cam *AFTER* destroying the interactive one
        parent.switchCamTo(theCam.getActiveSettings());

        // store modified centerpoint
        entity.setCenterpointSettings(((CRadishInteractiveRotatingCamera)theCam).getCenterPointSettings());

        super.OnLeaveState(nextStateName);
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractiveStop"));
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        parent.notice(msg);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleInteractiveCam'));

        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "RAD_CamRotYaw"));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampY', "RAD_CamRotPitch"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_MouseWheel', "RAD_CamMoveForwardBack"));

        hotkeyList.PushBack(HotkeyHelp_from('RAD_MoveUpDown', "RAD_CenterMoveUpDown"));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_Back', "RAD_StopInteractiveCam"));
    }
    // ------------------------------------------------------------------------
    protected function createCam() : CRadishInteractiveCamera {
        var cam: CRadishInteractiveRotatingCamera;
        var centerPoint: Vector;

        cam = (CRadishInteractiveRotatingCamera)createAndSetupInteractiveCam(
            parent.getConfig(), parent.getCamPlacement(), parent.getCamTracker());

        centerPoint = entity.getCenterpointSettings();
        if (centerPoint.W == 0) {
            // set default distance
            centerPoint.W = 1.5;
        }
        cam.setCenterPointSettings(centerPoint);

        cam.minPitch = -45.0;
        cam.maxPitch = 75.0;
        cam.minDistance = 0.2;

        cam.setFov(25.0);

        return cam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
