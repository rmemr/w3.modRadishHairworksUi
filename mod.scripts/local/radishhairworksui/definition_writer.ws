// ----------------------------------------------------------------------------
enum EHWUI_MapType {
    EHWUIMT_VALUE = 0,
    EHWUIMT_REF = 1,
}
// ----------------------------------------------------------------------------
struct SHWUI_ReferenceId {
    var id: String;
    var valueIdPrefix: String;
}
// ----------------------------------------------------------------------------
struct SHWUI_ValueId {
    var cat: String;
    var id: String;
}
// ----------------------------------------------------------------------------
struct SHWUI_VarMapping {
    var id: String;
    var type: EHWUI_MapType;
    var value: SHWUI_ValueId;
    var reference: SHWUI_ReferenceId;
}
// ----------------------------------------------------------------------------
struct SHWUI_ClassMapping {
    var id: String;
    var values: array<SHWUI_VarMapping>;
}
// ----------------------------------------------------------------------------
// maps yaml definition structure supported by encoder to ids from envui
class CModHairworksUiDefinitionMapper {
    // ------------------------------------------------------------------------
    private var mappings: array<SHWUI_ClassMapping>;
    // ------------------------------------------------------------------------
    public function init() {
        add_root();
    }
    // ------------------------------------------------------------------------
    private function add_root() {
        add_physicalMaterials();
        add_graphicalMaterials();
        add_levelOfDetails();
    }
    // ------------------------------------------------------------------------
    private function add_physicalMaterials() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurPhysicalMaterials");
        group.values.Clear();
        group.values.PushBack(SHWUI_VarMapping("simulation", EHWUIMT_REF,, SHWUI_ReferenceId("SFurSimulation")));
        group.values.PushBack(SHWUI_VarMapping("volume", EHWUIMT_REF,, SHWUI_ReferenceId("SFurVolume")));
        group.values.PushBack(SHWUI_VarMapping("strandWidth", EHWUIMT_REF,, SHWUI_ReferenceId("SFurStrandWidth")));
        group.values.PushBack(SHWUI_VarMapping("stiffness", EHWUIMT_REF,, SHWUI_ReferenceId("SFurStiffness")));
        group.values.PushBack(SHWUI_VarMapping("clumping", EHWUIMT_REF,, SHWUI_ReferenceId("SFurClumping")));
        group.values.PushBack(SHWUI_VarMapping("waveness", EHWUIMT_REF,, SHWUI_ReferenceId("SFurWaveness")));
        mappings.PushBack(group);

        add_physicalSimulation();
        add_physicalVolume();
        add_physicalStrandWith();
        add_physicalStiffness();
        add_physicalClumping();
        add_physicalWaveness();
    }
    // ------------------------------------------------------------------------
    private function add_graphicalMaterials() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurGraphicalMaterials");
        group.values.Clear();
        group.values.PushBack(SHWUI_VarMapping("color", EHWUIMT_REF,, SHWUI_ReferenceId("SFurColor")));
        group.values.PushBack(SHWUI_VarMapping("diffuse", EHWUIMT_REF,, SHWUI_ReferenceId("SFurDiffuse")));
        group.values.PushBack(SHWUI_VarMapping("specular", EHWUIMT_REF,, SHWUI_ReferenceId("SFurSpecular")));
        group.values.PushBack(SHWUI_VarMapping("glint", EHWUIMT_REF,, SHWUI_ReferenceId("SFurGlint")));
        group.values.PushBack(SHWUI_VarMapping("shadow", EHWUIMT_REF,, SHWUI_ReferenceId("SFurShadow")));
        mappings.PushBack(group);

        add_materialColor();
        add_materialDiffuse();
        add_materialSpecular();
        add_materialGlint();
        add_materialShadow();
    }
    // ------------------------------------------------------------------------
    private function add_physicalSimulation() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurSimulation");
        group.values.Clear();
        cat1 = "physical:simulation";
        group.values.PushBack(SHWUI_VarMapping("activated", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "activated")));
        group.values.PushBack(SHWUI_VarMapping("massScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "massScale")));
        group.values.PushBack(SHWUI_VarMapping("damping", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "damping")));
        group.values.PushBack(SHWUI_VarMapping("friction", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "friction")));
        group.values.PushBack(SHWUI_VarMapping("backStopRadius", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "backStopRadius")));
        group.values.PushBack(SHWUI_VarMapping("inertiaScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "inertiaScale")));
        group.values.PushBack(SHWUI_VarMapping("inertiaLimit", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "inertiaLimit")));
        group.values.PushBack(SHWUI_VarMapping("useCollision", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "useCollision")));
        group.values.PushBack(SHWUI_VarMapping("windScaler", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "windScaler")));
        group.values.PushBack(SHWUI_VarMapping("windNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "windNoise")));
        group.values.PushBack(SHWUI_VarMapping("gravityDir", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "gravityDir")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_physicalVolume() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurVolume");
        group.values.Clear();
        cat1 = "physical:volume";
        group.values.PushBack(SHWUI_VarMapping("density", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "density")));
        group.values.PushBack(SHWUI_VarMapping("densityTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "densityTex")));
        group.values.PushBack(SHWUI_VarMapping("densityTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "densityTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("usePixelDensity", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "usePixelDensity")));
        group.values.PushBack(SHWUI_VarMapping("lengthScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "lengthScale")));
        group.values.PushBack(SHWUI_VarMapping("lengthNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "lengthNoise")));
        group.values.PushBack(SHWUI_VarMapping("lengthTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "lengthTex")));
        group.values.PushBack(SHWUI_VarMapping("lengthTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "lengthTexChannel")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_physicalStrandWith() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurStrandWidth");
        group.values.Clear();
        cat1 = "physical:strand width";
        group.values.PushBack(SHWUI_VarMapping("width", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "width")));
        group.values.PushBack(SHWUI_VarMapping("widthRootScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "widthRootScale")));
        group.values.PushBack(SHWUI_VarMapping("widthTipScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "widthTipScale")));
        group.values.PushBack(SHWUI_VarMapping("rootWidthTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootWidthTex")));
        group.values.PushBack(SHWUI_VarMapping("rootWidthTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootWidthTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("tipWidthTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "tipWidthTex")));
        group.values.PushBack(SHWUI_VarMapping("tipWidthTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "tipWidthTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("widthNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "widthNoise")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_physicalStiffness() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurStiffness");
        group.values.Clear();
        cat1 = "physical:stiffness";
        group.values.PushBack(SHWUI_VarMapping("stiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffness")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessStrength", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessStrength")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessTex")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("interactionStiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "interactionStiffness")));
        group.values.PushBack(SHWUI_VarMapping("pinStiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "pinStiffness")));
        group.values.PushBack(SHWUI_VarMapping("rootStiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootStiffness")));
        group.values.PushBack(SHWUI_VarMapping("rootStiffnessTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootStiffnessTex")));
        group.values.PushBack(SHWUI_VarMapping("rootStiffnessTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootStiffnessTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessDamping", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessDamping")));
        group.values.PushBack(SHWUI_VarMapping("tipStiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "tipStiffness")));
        group.values.PushBack(SHWUI_VarMapping("bendStiffness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "bendStiffness")));
        //group.values.PushBack(SHWUI_VarMapping("stiffnessBoneEnable", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessBoneEnable")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessStartDistance", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessStartDistance")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessEndDistance", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessEndDistance")));
        //group.values.PushBack(SHWUI_VarMapping("stiffnessBoneCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessBoneCurve")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessCurve")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessStrengthCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessStrengthCurve")));
        group.values.PushBack(SHWUI_VarMapping("stiffnessDampingCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "stiffnessDampingCurve")));
        group.values.PushBack(SHWUI_VarMapping("bendStiffnessCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "bendStiffnessCurve")));
        group.values.PushBack(SHWUI_VarMapping("interactionStiffnessCurve", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "interactionStiffnessCurve")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_physicalClumping() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurClumping");
        group.values.Clear();
        cat1 = "physical:clumping";
        group.values.PushBack(SHWUI_VarMapping("clumpScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpScale")));
        group.values.PushBack(SHWUI_VarMapping("clumpScaleTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpScaleTex")));
        group.values.PushBack(SHWUI_VarMapping("clumpScaleTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpScaleTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("clumpRoundness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpRoundness")));
        group.values.PushBack(SHWUI_VarMapping("clumpRoundnessTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpRoundnessTex")));
        group.values.PushBack(SHWUI_VarMapping("clumpRoundnessTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpRoundnessTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("clumpNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpNoise")));
        group.values.PushBack(SHWUI_VarMapping("clumpNoiseTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpNoiseTex")));
        group.values.PushBack(SHWUI_VarMapping("clumpNoiseTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "clumpNoiseTexChannel")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_physicalWaveness() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurWaveness");
        group.values.Clear();
        cat1 = "physical:waveness";
        group.values.PushBack(SHWUI_VarMapping("waveScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveScale")));
        group.values.PushBack(SHWUI_VarMapping("waveScaleTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveScaleTex")));
        group.values.PushBack(SHWUI_VarMapping("waveScaleTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveScaleTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("waveScaleNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveScaleNoise")));
        group.values.PushBack(SHWUI_VarMapping("waveFreq", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveFreq")));
        group.values.PushBack(SHWUI_VarMapping("waveFreqTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveFreqTex")));
        group.values.PushBack(SHWUI_VarMapping("waveFreqTexChannel", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveFreqTexChannel")));
        group.values.PushBack(SHWUI_VarMapping("waveFreqNoise", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveFreqNoise")));
        group.values.PushBack(SHWUI_VarMapping("waveRootStraighten", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveRootStraighten")));
        group.values.PushBack(SHWUI_VarMapping("waveStrand", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveStrand")));
        group.values.PushBack(SHWUI_VarMapping("waveClump", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "waveClump")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_materialColor() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurColor");
        group.values.Clear();
        cat1 = "material:color";
        group.values.PushBack(SHWUI_VarMapping("rootAlphaFalloff", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootAlphaFalloff")));
        group.values.PushBack(SHWUI_VarMapping("rootColor", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootColor")));
        group.values.PushBack(SHWUI_VarMapping("rootColorTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootColorTex")));
        group.values.PushBack(SHWUI_VarMapping("tipColor", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "tipColor")));
        group.values.PushBack(SHWUI_VarMapping("tipColorTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "tipColorTex")));
        group.values.PushBack(SHWUI_VarMapping("rootTipColorWeight", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootTipColorWeight")));
        group.values.PushBack(SHWUI_VarMapping("rootTipColorFalloff", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "rootTipColorFalloff")));
        group.values.PushBack(SHWUI_VarMapping("strandTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "strandTex")));
        group.values.PushBack(SHWUI_VarMapping("strandBlendMode", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "strandBlendMode")));
        group.values.PushBack(SHWUI_VarMapping("strandBlendScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "strandBlendScale")));
        group.values.PushBack(SHWUI_VarMapping("textureBrightness", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "textureBrightness")));
        group.values.PushBack(SHWUI_VarMapping("ambientEnvScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "ambientEnvScale")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_materialDiffuse() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurDiffuse");
        group.values.Clear();
        cat1 = "material:diffuse";
        group.values.PushBack(SHWUI_VarMapping("diffuseBlend", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseBlend")));
        group.values.PushBack(SHWUI_VarMapping("diffuseScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseScale")));
        group.values.PushBack(SHWUI_VarMapping("diffuseHairNormalWeight", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseHairNormalWeight")));
        group.values.PushBack(SHWUI_VarMapping("diffuseBoneLocalPos", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseBoneLocalPos")));
        group.values.PushBack(SHWUI_VarMapping("diffuseNoiseScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseNoiseScale")));
        group.values.PushBack(SHWUI_VarMapping("diffuseNoiseFreqU", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseNoiseFreqU")));
        group.values.PushBack(SHWUI_VarMapping("diffuseNoiseFreqV", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseNoiseFreqV")));
        group.values.PushBack(SHWUI_VarMapping("diffuseNoiseGain", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "diffuseNoiseGain")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_materialSpecular() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurSpecular");
        group.values.Clear();
        cat1 = "material:specular";
        group.values.PushBack(SHWUI_VarMapping("specularColor", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularColor")));
        group.values.PushBack(SHWUI_VarMapping("specularTex", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularTex")));
        group.values.PushBack(SHWUI_VarMapping("specularPrimary", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularPrimary")));
        group.values.PushBack(SHWUI_VarMapping("specularPowerPrimary", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularPowerPrimary")));
        group.values.PushBack(SHWUI_VarMapping("specularPrimaryBreakup", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularPrimaryBreakup")));
        group.values.PushBack(SHWUI_VarMapping("specularSecondary", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularSecondary")));
        group.values.PushBack(SHWUI_VarMapping("specularPowerSecondary", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularPowerSecondary")));
        group.values.PushBack(SHWUI_VarMapping("specularSecondaryOffset", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularSecondaryOffset")));
        group.values.PushBack(SHWUI_VarMapping("specularNoiseScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularNoiseScale")));
        group.values.PushBack(SHWUI_VarMapping("specularEnvScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "specularEnvScale")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_materialGlint() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurGlint");
        group.values.Clear();
        cat1 = "material:glint";
        group.values.PushBack(SHWUI_VarMapping("glintStrength", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "glintStrength")));
        group.values.PushBack(SHWUI_VarMapping("glintCount", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "glintCount")));
        group.values.PushBack(SHWUI_VarMapping("glintExponent", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "glintExponent")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_materialShadow() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurShadow");
        group.values.Clear();
        cat1 = "material:shadow";
        group.values.PushBack(SHWUI_VarMapping("shadowSigma", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "shadowSigma")));
        group.values.PushBack(SHWUI_VarMapping("shadowDensityScale", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "shadowDensityScale")));
        group.values.PushBack(SHWUI_VarMapping("castShadows", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "castShadows")));
        group.values.PushBack(SHWUI_VarMapping("receiveShadows", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "receiveShadows")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_levelOfDetails() {
        var group: SHWUI_ClassMapping;
        var cat1: String;

        group = SHWUI_ClassMapping("SFurLevelOfDetail");
        group.values.Clear();
        cat1 = "level of detail";
        group.values.PushBack(SHWUI_VarMapping("activated", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "activated")));
        group.values.PushBack(SHWUI_VarMapping("useViewfrustrumCulling", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "useViewfrustrumCulling")));
        group.values.PushBack(SHWUI_VarMapping("useBackfaceCulling", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "useBackfaceCulling")));
        group.values.PushBack(SHWUI_VarMapping("backfaceCullingThreshold", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "backfaceCullingThreshold")));

        group.values.PushBack(SHWUI_VarMapping("enableDistanceLOD", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "enableDistanceLOD")));
        group.values.PushBack(SHWUI_VarMapping("distanceLODStart", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "distanceLODStart")));
        group.values.PushBack(SHWUI_VarMapping("distanceLODEnd", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "distanceLODEnd")));
        group.values.PushBack(SHWUI_VarMapping("distanceLODFadeStart", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "distanceLODFadeStart")));
        group.values.PushBack(SHWUI_VarMapping("distanceLODWidth", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "distanceLODWidth")));
        group.values.PushBack(SHWUI_VarMapping("distanceLODDensity", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "distanceLODDensity")));

        group.values.PushBack(SHWUI_VarMapping("enableDetailLOD", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "enableDetailLOD")));
        group.values.PushBack(SHWUI_VarMapping("detailLODStart", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "detailLODStart")));
        group.values.PushBack(SHWUI_VarMapping("detailLODEnd", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "detailLODEnd")));
        group.values.PushBack(SHWUI_VarMapping("detailLODWidth", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "detailLODWidth")));
        group.values.PushBack(SHWUI_VarMapping("detailLODDensity", EHWUIMT_VALUE, SHWUI_ValueId(cat1, "detailLODDensity")));

        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    public function get(groupRef: SHWUI_ReferenceId) : SHWUI_ClassMapping {
        var i, s: int;

        s = mappings.Size();
        for (i = 0; i < s; i += 1) {
            if (mappings[i].id == groupRef.id) {
                return mappings[i];
            }
        }

        return SHWUI_ClassMapping();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModHairworksUiDefinitionWriter {
    // ------------------------------------------------------------------------
    private var mapper: CModHairworksUiDefinitionMapper;
    // ------------------------------------------------------------------------
    public function init() {
        mapper = new CModHairworksUiDefinitionMapper in this;
        mapper.init();
    }
    // ------------------------------------------------------------------------
    private function encodeSpheres(cat1: String, idPrefix: String, fur: CModHairworksUiParams) : SEncValue {
        var group, sphere: SEncValue;
        var pos, radius: SHWUI_Value;

        var i, s: int;
        var attributeMapping: SHWUI_VarMapping;

        group = encValueNewMap();

        for (i = 0; i < 10; i += 1) {

            pos = fur.getValue(cat1, idPrefix + "[" + IntToString(i) + "].localpos");
            radius = fur.getValue(cat1, idPrefix + "[" + IntToString(i) + "].radius");

            if (pos.vec != Vector() || radius.f != 0.0) {
                sphere = encValueNewMap();
                encMapPush("localpos", Pos4ToEncValue(pos.vec), sphere);
                encMapPush("radius", FloatToEncValue(radius.f), sphere);

                encMapPush(idPrefix + "[" + IntToString(i) + "]", sphere, group);
            }
        }

        if (group.m.Size() > 0) {
            return group;
        } else {
            return SEncValue(EEVT_Null);
        }
    }
    // ------------------------------------------------------------------------
    private function encodeGroup(groupId: SHWUI_ReferenceId, fur: CModHairworksUiParams) : SEncValue {
        var group, value: SEncValue;
        var i, s: int;
        var mapping: SHWUI_ClassMapping;
        var attributeMapping: SHWUI_VarMapping;

        group = encValueNewMap();

        mapping = mapper.get(groupId);
        s = mapping.values.Size();
        for (i = 0; i < s; i += 1) {
            attributeMapping = mapping.values[i];

            if (attributeMapping.type == EHWUIMT_REF) {
                // special case for pins and bonespheres
                switch (attributeMapping.reference.id) {
                    case "SPinConstraints": value = encodeSpheres("pins", "pins", fur); break;
                    case "SBoneSpheres":    value = encodeSpheres("bonespheres", "bonesphere", fur); break;
                    default:                value = encodeGroup(attributeMapping.reference, fur);
                }
            } else {
                attributeMapping.value.id = groupId.valueIdPrefix + attributeMapping.value.id;
                value = encodeValue(attributeMapping.value, fur);
            }
            encMapPush(attributeMapping.id, value, group);
        }
        if (group.m.Size() > 0) {
            return group;
        } else {
            return SEncValue(EEVT_Null);
        }
    }
    // ------------------------------------------------------------------------
    private function encodeValue(valueId: SHWUI_ValueId, fur: CModHairworksUiParams) : SEncValue {
        var value: SHWUI_Value;

        value = fur.getValue(valueId.cat, valueId.id);

        switch (value.type) {
            case EHWUI_FLOAT: return FloatToEncValue(value.f);
            case EHWUI_BOOL: return BoolToEncValue(value.b);
            case EHWUI_INT: return IntToEncValue(value.i);
            case EHWUI_COLOR: return ColorToEncValue(value.c);
            case EHWUI_VECTOR: return Pos4ToEncValue(value.vec);
            case EHWUI_STRING: return StrToEncValue(value.s);
        }

        LogChannel('W3FUR', "found INVALID value type [" + value.type + "] for " + valueId.cat + ":" + valueId.id);
        return SEncValue();
    }
    // ------------------------------------------------------------------------
    private function ColorToEncValue(col: SHWUI_Color) : SEncValue {
        var r: SEncValue;
        r.type = EEVT_List;
        r.l.PushBack(IntToEncValue(col.r));
        r.l.PushBack(IntToEncValue(col.g));
        r.l.PushBack(IntToEncValue(col.b));
        r.l.PushBack(IntToEncValue(col.a));

        return r;
    }
    // ------------------------------------------------------------------------
    public function logDefinition(fur: CModHairworksUiParams) {
        var definitionWriter: CRadishDefinitionWriter;
        var root, params, dryParams, wetParams: SEncValue;
        var i: int;

        fur.setWetnessMaterialSet(true);
        wetParams = encValueNewMap();
        encMapPush("physical", encodeGroup(SHWUI_ReferenceId("SFurPhysicalMaterials"), fur), wetParams);
        encMapPush("material", encodeGroup(SHWUI_ReferenceId("SFurGraphicalMaterials"), fur), wetParams);

        fur.setWetnessMaterialSet(false);
        dryParams = encValueNewMap();
        encMapPush("physical", encodeGroup(SHWUI_ReferenceId("SFurPhysicalMaterials"), fur), dryParams);
        encMapPush("material", encodeGroup(SHWUI_ReferenceId("SFurGraphicalMaterials"), fur), dryParams);

        params = encValueNewMap();
        encMapPush("autohidedistance", encodeValue(SHWUI_ValueId("", "autoHideDistance"), fur), params);
        encMapPush("dry", dryParams, params);
        encMapPush("wet", wetParams, params);

        encMapPush("lod", encodeGroup(SHWUI_ReferenceId("SFurLevelOfDetail"), fur), params);
        encMapPush("pins", encodeGroup(SHWUI_ReferenceId("SPinConstraints"), fur), params);
        encMapPush("bonespheres", encodeGroup(SHWUI_ReferenceId("SBoneSpheres"), fur), params);

        root = encValueNewMap();
        encMapPush("furparameter", params, root);

        definitionWriter = new CRadishDefinitionWriter in this;
        definitionWriter.create('W3FUR', "Hairworks Ui", root);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
