// ----------------------------------------------------------------------------
// DO NOT CHANGE PROPERTY ORDER (important for initialization!)
// ----------------------------------------------------------------------------
struct SRadishHwConfig {
    var cam: SRadishCamConfig;
    var placement: SRadishPlacementConfig;
}
// ----------------------------------------------------------------------------
class CRadishHairworksUiConfigManager extends IRadishConfigManager {
    protected var config: SRadishHwConfig;
    // ------------------------------------------------------------------------
    public function init() {
        config = getDefaults();
    }
    // ------------------------------------------------------------------------
    private function getDefaults() : SRadishHwConfig {
        return SRadishHwConfig(
            // DO NOT CHANGE ORDER (important for initialization!)
            SRadishCamConfig(
                SRadishInteractiveStepConfig(0.005, 0.05),
                SRadishInteractiveStepConfig(0.025, 0.20),
                SRadishInteractiveStepConfig(0.080, 0.40),
                true,
                RadUi_createCamSettingsFor(RadUiCam_Empty)
            ),
            SRadishPlacementConfig(
                SRadishInteractiveStepConfig(0.01, 0.1),
                SRadishInteractiveStepConfig(0.03, 0.2),
                SRadishInteractiveStepConfig(0.1, 0.3),
            )
        );
    }
    // ------------------------------------------------------------------------
    // setter
    // ------------------------------------------------------------------------
    public function setLastCamPosition(placement: SRadishPlacement) {
        config.cam.lastPos = placement;
    }
    // ------------------------------------------------------------------------
    // getter
    // ------------------------------------------------------------------------
    public function getLastCamPosition(): SRadishPlacement {
        return config.cam.lastPos;
    }
    // ------------------------------------------------------------------------
    public function getCamConfig() : SRadishCamConfig {
        return config.cam;
    }
    // ------------------------------------------------------------------------
    public function getPlacementConfig() : SRadishPlacementConfig {
        return config.placement;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
