// ----------------------------------------------------------------------------
function RadHwUi_createCamSettingsFor(entity: CModHairworksUiFurEntity) : SRadishPlacement {
    var s: SRadishPlacement;
    var distance: float;
    var centerPoint, initialCamPos: Vector;
    var newPos, direction: Vector;
    var newRot: EulerAngles;

    centerPoint = entity.getCenterpointSettings();

    initialCamPos = centerPoint;
    initialCamPos.Z += 0.25;
    initialCamPos.X += 1.0;
    initialCamPos.Y += 1.0;

    direction = initialCamPos - centerPoint;
    newRot = VecToRotation(direction);
    newRot.Pitch *= -1;

    // set default distance
    distance = 1.5;
    newPos = VecTransform(
        MatrixBuiltTranslation(centerPoint) *
        MatrixBuiltRotation(newRot),
        Vector(0, distance, 0)
    );
    newRot = VecToRotation(centerPoint - newPos);
    newRot.Pitch *= -1;

    s.pos = newPos;
    s.rot = newRot;

    return s;
}
// ----------------------------------------------------------------------------
